﻿using BLL;
using BLL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TICA.ViewModels
{
    public class AduanaRecinto
    {
        string ObtenerRecintosMaritimo = "http://192.168.6.180:8080/devrenoir/tsmwww/AutoTICA/ObtenerRecintosMaritimo/";
        public AduanaRecinto() 
        {
            var listaRecintos = Helpers.ObtenerRespuestJsonWebService("GET", ObtenerRecintosMaritimo, string.Empty);

            Options = new List<RecintosMaritimo>();
            foreach (var recinto in listaRecintos)
            {
                Options.Add(recinto);
            }
        }

        public List<RecintosMaritimo> Options { get; set; } 
    }
}
