﻿using OpenQA.Selenium;
//using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BLL;
using OpenQA.Selenium.Support.UI;
using BLL.Models;
using OpenQA.Selenium.Chrome;
using System.Threading;
using Microsoft.Edge.SeleniumTools;
using System.IO;
using Path = System.IO.Path;
using System.Diagnostics;
using OpenQA.Selenium.Remote;
using Microsoft.Win32;

namespace pruebaTICA
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        FlowDocument flowDocument = new FlowDocument();
        Paragraph newParagraph = new Paragraph();

        #region URLs for webservices

        string eliminarLineasAereoManifiestos = "http://192.168.6.180:8080/devrenoir/tsmwww/AutoTICA/eliminarLineasAereoManifiestos/";
        string eliminarLineasViajesMaritimo = "http://192.168.6.180:8080/devrenoir/tsmwww/AutoTICA/eliminarLineasMaritimoViajes/";
        string eliminarLineasViajesAereo = "http://192.168.6.180:8080/devrenoir/tsmwww/AutoTICA/eliminarLineasAereoViajes/";
        string ObtenerRecintosMaritimo = "http://192.168.6.180:8080/devrenoir/tsmwww/AutoTICA/ObtenerRecintosMaritimo/";
        string EliminarViajesMaritimoReanudacion = "http://192.168.6.180:8080/devrenoir/tsmwww/AutoTICA/EliminarViajesMaritimoReanudacion/";
        string EliminarViajesAereoReanudacion = "http://192.168.6.180:8080/devrenoir/tsmwww/AutoTICA/EliminarViajesAereoReanudacion/";
        string EliminarManifiestoAereoReanudacion = "http://192.168.6.180:8080/devrenoir/tsmwww/AutoTICA/EliminarManifiestoAereoReanudacion/";
        string EliminarListaViajesMaritimo = "http://192.168.6.180:8080/devrenoir/tsmwww/AutoTICA/EliminarListaViajesMaritimo/";
        string EliminarListaViajesAereo = "http://192.168.6.180:8080/devrenoir/tsmwww/AutoTICA/EliminarListaViajesAereo/";
        string EliminarListaManifiestosAereo = "http://192.168.6.180:8080/devrenoir/tsmwww/AutoTICA/EliminarListaManifiestosAereo/";
        string ObtenerListaTotalViajesAereos = "http://192.168.6.180:8080/devrenoir/tsmwww/AutoTICA/ObtenerListaTotalViajesAereos/";
        string ObtenerListaTotalViajesMaritimos = "http://192.168.6.180:8080/devrenoir/tsmwww/AutoTICA/ObtenerListaTotalViajesMaritimos/";


        #endregion

        #region propiedades
        //Recinto Origen para reporte Aereo de viajes puede ser vacio o tener un valor dependiendo de si el checkbox está seleccionado o no. 
        string recintoOrigenAereo = string.Empty;
        #endregion


        public MainWindow()
        {
            InitializeComponent();
            dpDesde.SelectedDate = DateTime.Now.Date.AddDays(-10);
            dpHasta.SelectedDate = DateTime.Now.Date;
            WatchTicaLogsUsuario();
            Logger.EliminarLogFile(Constantes.TicaLogger);
        }

        public async void StartWeb(object sender, RoutedEventArgs e)
        {
            //Se obtiene el ID, que es un numero random, para cada vez que se corra el reporte y se pueda identificar el mismo en los logs por si hay errores
            var rnd = new Random();
            var runID = rnd.Next(1, 1000000);
            try
            {
                //Se realiza una busqueda para aereo
                var fechaInicio = dpDesde.SelectedDate.Value.ToString("dd/MM/yy", System.Globalization.CultureInfo.InvariantCulture);
                var fechaFinal = dpHasta.SelectedDate.Value.ToString("dd/MM/yy", System.Globalization.CultureInfo.InvariantCulture);

                #region Maritimo Viajes
                if (TipoCB.SelectedIndex==0)
                {
                    limpiarLog();
                    //Elimina el archivo creado cuando se corrio el reporte anteriormente
                    Helpers.EliminarResumenNumeroViajes("ViajesMaritimo");
                    EscribirLineaLog("Corriendo reporte Marítimo");
                    bool reanudacion = true;
                    reanudacion = Reanudacion.IsChecked.Value;
                    //Se encarga de limpiar la tabla de viajes maritimo.
                    if (eliminaInformacionDBCheckBox.IsChecked==true)
                    {
                        if (MessageBoxResult.OK == ConfirmaEliminacionInformacionExistente("Viajes Marítimo"))
                        {
                            Helpers.LLamarWebService("POST", eliminarLineasViajesMaritimo, string.Empty);
                        }
                        else
                        {
                            Helpers.EscribirLogsVisiblesUsuario("Proceso cancelado");
                            return;
                        }
                    }
                    IWebDriver driver = Helpers.LaunchDriver();
                    driver.Manage().Cookies.DeleteAllCookies();
                    Thread.Sleep(3000);
                    WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));

                    if (fechaInicio =="" || fechaFinal =="")
                    {
                        MessageBox.Show("Ambas fechas deben ser ingresadas");
                        return;
                    }
                    //Se obtienen los recintos
                    var listaRecintos = Helpers.ObtenerRespuestJsonWebService("GET", ObtenerRecintosMaritimo, string.Empty);
                    //Se llena el dropdown donde se hacen disponibles los viajes 

                    if (listaRecintos.Count==0)
                    {
                        MessageBox.Show("No existen recintos de ");
                    }
                    //Helpers.ObtenerRespuestJsonWebService("GET", ObtenerRecintosMaritimo, string.Empty);
                    string[] aduanasOrigen = {"006", "002"};

                   
                    await Task.Run(() =>
                    {
                        Logic logic = new Logic(runID, driver, wait);
                        driver.Navigate().GoToUrl("https://www.hacienda.go.cr/TICA/web/hvjentreadu.aspx");
                        wait = new WebDriverWait(driver, TimeSpan.FromSeconds(30));
                        //Se elimina el ultimo viaje procesado en caso de que sea un proceso de reanudacion de una extraccion anterior que no ha terminado o está pendiente. 
                        if (reanudacion)
                        {
                            var parametrosSalida = Helpers.ObtenerParametrosSalidaWebService("POST", EliminarViajesMaritimoReanudacion, 1);
                            if (parametrosSalida != null)
                            {
                                if (parametrosSalida.GetType() == typeof(ViajesJSON))
                                {
                                    var viaje = (ViajesJSON)parametrosSalida;
                                    if (viaje.viaje_p != String.Empty)
                                    {
                                        Helpers.EscribirLogsVisiblesUsuario($"Útimo viaje procesado: {viaje.viaje_p}. Este será eliminado y procesado de nuevo para asegurar la integridad de los datos.");
                                    }
                                }
                            }
                        }
                        else if (!reanudacion)
                        {
                            //Se elimina la lista de viajes existente para volver a obtenerla
                            Helpers.LLamarWebService("POST", EliminarListaViajesMaritimo, string.Empty);
                            foreach (var aduanaOr in aduanasOrigen)
                            {
                                Helpers.EscribirLogsVisiblesUsuario($"Verificando lista total de viajes a procesar de las fechas {fechaInicio} a {fechaFinal} para aduana origen {aduanaOr}. Esto puede tardar unos minutos...");
                                logic.BuscarViajesMaritimo(aduanaOr, fechaInicio, fechaFinal, string.Empty, false);
                                Thread.Sleep(60000);
                                ProcesadorViajesExcel procesadorViajesExcel = new ProcesadorViajesExcel(driver, wait);
                                procesadorViajesExcel.TipoReporte = "Maritimo";
                                procesadorViajesExcel.listaRecintosMaritimo = listaRecintos;
                                var archivoDescargado = procesadorViajesExcel.DescargarArchivoExcel();
                                if (archivoDescargado)
                                {
                                    List<ViajeInformacionGeneral> listaViajes = procesadorViajesExcel.LeerArchivo();
                                    bool viajesGuardadosExitosamente = procesadorViajesExcel.GuardarListaViajes(listaViajes);
                                    if (viajesGuardadosExitosamente)
                                    {
                                        Thread.Sleep(1000);
                                        Helpers.EscribirLogsVisiblesUsuario($"Total de viajes a ser procesados para aduana de origen {aduanaOr}: {listaViajes.Count}");
                                    }
                                }
                                else
                                {
                                    Helpers.EscribirLogsVisiblesUsuario("Problema al descargar el archivo que contiene los viajes a ser procesados.");
                                    return;
                                }
                            }
                        }
                        
                        foreach (var aduanaOr in aduanasOrigen)
                        {
                            foreach (var recinto in listaRecintos)
                            {
                                //if (recinto.rm_origen== "A234" && aduanaOr=="006")
                                //{
                                    //Se obtiene la cantidad de viajes por recinto de destino que se esté procesando. 
                                    var url = ObtenerListaTotalViajesMaritimos + $"/{aduanaOr}/{recinto.rm_origen}/";
                                    var listaViajes = Helpers.ObtenerInformacionViajeListaTotalMaritimoJsonWebServicePaginacion("GET", url);
                                    var listaViajesFchFormateada = Helpers.ObtenerListaViajesMaritimoFechasFormateadas(listaViajes);
                                    var lstFechas = listaViajesFchFormateada.GroupBy(x => x.lvm_fecha).Distinct().ToList();
                                    var cont = 0;
                                    foreach (var fch in lstFechas)
                                    {
                                        //Si la fecha que se va a buscar ya tiene todos los viajes procesados, no se realiza el proceso de nuevo ya que no es necesario. 
                                        var viajesPendientes = listaViajesFchFormateada.Where(f => f.lvm_fecha == fch.Key && f.lvm_procesado == "0").ToList();
                                        if (viajesPendientes.Count>0)
                                        {
                                            Helpers.EscribirLogsVisiblesUsuario($"Buscando viajes para Aduana Origen: {aduanaOr} y Recinto: {recinto.rm_origen} para fecha: {fch.Key}");
                                            logic.BuscarViajesMaritimo(aduanaOr, fch.Key, fch.Key, recinto.rm_origen, false);
                                            driver = logic.ProcesarResultadosMaritimo();
                                        }
                                        else
                                        {
                                            if (cont==0)
                                            {
                                                Helpers.EscribirLogsVisiblesUsuario($"Todos los viajes de la aduana {aduanaOr} y Recinto {recinto.rm_origen} han sido procesados anteriormente");
                                            }
                                        }
                                        cont++;
    
                                    }
                                //}
                                
                            }

                        }
                    });
                    Logger.EnviarLogsCorreo("Viajes Marítimo");
                    Logger.EliminarLogFile(Constantes.TicaLogger);
                    Helpers.EscribirLogsVisiblesUsuario("Extracción de viajes marítimo finalizado con éxito.");

                }
                #endregion

                #region Aereo Viajes
                if (TipoCB.SelectedIndex==1)
                {
                    limpiarLog();
                    bool reanudacion = true;
                    reanudacion = Reanudacion.IsChecked.Value;
                    EscribirLineaLog("Corriendo reporte Aéreo de viajes");
                    //Se encarga de limpiar la tabla de viajes aereo.
                    if (eliminaInformacionDBCheckBox.IsChecked == true)
                    {
                        if (MessageBoxResult.OK == ConfirmaEliminacionInformacionExistente("Viajes Aéreo"))
                        {
                            Helpers.LLamarWebService("POST", eliminarLineasViajesAereo, string.Empty);
                        }
                        else
                        {
                            Helpers.EscribirLogsVisiblesUsuario("Proceso cancelado");
                            return;
                        }
                    }

                    IWebDriver driver = Helpers.LaunchDriver();
                    driver.Manage().Cookies.DeleteAllCookies();
                    Thread.Sleep(3000);
                    WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(30));
                    Task reporteAereoViajes = new Task(() =>
                    {
                        driver.Navigate().GoToUrl("https://www.hacienda.go.cr/TICA/web/hvjentreadu.aspx");
                        wait = new WebDriverWait(driver, TimeSpan.FromSeconds(30));
                        Logic logic = new Logic(runID, driver, wait);
                        //Reporte Aereo viajes
                        logic.BuscarViajesAereo(fechaInicio, fechaFinal, recintoOrigenAereo);
                        //Se elimina el ultimo viaje procesado en caso de que sea un proceso de reanudacion de una extraccion anterior que no ha terminado o está pendiente. 
                        if (!reanudacion)
                        {
                            //Se elimina la lista de viajes existente para volver a obtenerla
                            Helpers.LLamarWebService("POST", EliminarListaViajesAereo, string.Empty);

                            Helpers.EscribirLogsVisiblesUsuario($"Verificando lista total de viajes a procesar de las fechas {fechaInicio} a {fechaFinal}. Esto puede tardar unos minutos...");
                            Thread.Sleep(60000);
                            ProcesadorViajesExcel procesadorViajesExcel = new ProcesadorViajesExcel(driver, wait);
                            procesadorViajesExcel.TipoReporte = "Aereo";
                            var archivoDescargado = procesadorViajesExcel.DescargarArchivoExcel();
                            if (archivoDescargado)
                            {
                                List<ViajeInformacionGeneral> listaViajes = procesadorViajesExcel.LeerArchivo();
                                bool viajesGuardadosExitosamente = procesadorViajesExcel.GuardarListaViajes(listaViajes);
                                Helpers.EscribirLogsVisiblesUsuario($"Total de viajes a ser procesados: {listaViajes.Count}");
                                //Se obtiene la lista de viajes con su fecha 
                                var viajesAereos = Helpers.ObtenerInformacionViajeListaTotalAereoJsonWebServicePaginacion("GET", ObtenerListaTotalViajesAereos);
                                driver = logic.ProcesarResultadosAereo(viajesAereos);
                            }
                        }
                        else if (reanudacion)
                        {
                            //Se obtiene la lista de viajes con su fecha 
                            var viajesAereos = Helpers.ObtenerInformacionViajeListaTotalAereoJsonWebServicePaginacion("GET", ObtenerListaTotalViajesAereos);
                            var parametrosSalida = Helpers.ObtenerParametrosSalidaWebService("POST", EliminarViajesAereoReanudacion, 1);
                            if (parametrosSalida != null)
                            {
                                if (parametrosSalida.GetType() == typeof(ViajesJSON))
                                {
                                    var viaje = (ViajesJSON)parametrosSalida;
                                    if (viaje.viaje_p != String.Empty)
                                    {
                                        Helpers.EscribirLogsVisiblesUsuario($"Útimo viaje procesado: {viaje.viaje_p}. Este será eliminado y procesado de nuevo para asegurar la integridad de los datos.");
                                    }
                                }
                            }
                            driver = logic.ProcesarResultadosAereo(viajesAereos);
                        }
                    });

                    reporteAereoViajes.Start();
                    await reporteAereoViajes;

                    Logger.EnviarLogsCorreo("Viajes Aéreo");
                    Logger.EliminarLogFile(Constantes.TicaLogger);
                    Helpers.EscribirLogsVisiblesUsuario("Extracción de viajes aéreo finalizado con éxito.");
                }
                #endregion

                #region Aereo Manifiestos
                if (TipoCB.SelectedIndex == 2)
                {
                    bool reanudacion = true;
                    reanudacion = Reanudacion.IsChecked.Value;
                    limpiarLog();
                    EscribirLineaLog("Corriendo reporte Aéreo manifiestos");
                    //Se encarga de limpiar la tabla de Aereo Manifiestos
                    if (eliminaInformacionDBCheckBox.IsChecked == true)
                    {
                        if (MessageBoxResult.OK == ConfirmaEliminacionInformacionExistente("Aéreo manifiestos"))
                        {
                            Helpers.LLamarWebService("POST", eliminarLineasAereoManifiestos, string.Empty);
                        }
                        else
                        {
                            Helpers.EscribirLogsVisiblesUsuario("Proceso cancelado");
                            return;
                        }
                    }
                    if (!reanudacion)
                    {
                        Helpers.LLamarWebService("POST", EliminarListaManifiestosAereo, string.Empty);
                    }
                    else
                    {
                        var parametrosSalida = Helpers.ObtenerParametrosSalidaWebService("POST", EliminarManifiestoAereoReanudacion, 1);
                        if (parametrosSalida != null)
                        {
                            if (parametrosSalida.GetType() == typeof(ViajesJSON))
                            {
                                var manifiesto = (ViajesJSON)parametrosSalida;
                                if (manifiesto.viaje_p!= String.Empty)
                                {
                                    Helpers.EscribirLogsVisiblesUsuario($"Útimo manifiesto procesado: {manifiesto.viaje_p}. Este será eliminado y procesado de nuevo para asegurar la integridad de los datos.");
                                }
                            }
                        }
                    }
                    IWebDriver driver = Helpers.LaunchDriver();
                    driver.Manage().Cookies.DeleteAllCookies();
                    Thread.Sleep(3000);
                    WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(30));
                    Task reporteAereoManifiesto = new Task(() =>
                    {
                        driver.Navigate().GoToUrl("https://www.hacienda.go.cr/tica/web/hcicgmic.aspx");
                        Logic logic = new Logic(runID, driver, wait);
                        //Reporte Aereo Manifiestos 
                        logic.BuscarManifiestosAereo(fechaInicio, fechaFinal);
                        logic.RecorrerTablaManifiestosAereo(reanudacion);
                    });

                    reporteAereoManifiesto.Start();
                    await reporteAereoManifiesto;

                    Logger.EnviarLogsCorreo("Aéreo Manifiestos");
                    Logger.EliminarLogFile(Constantes.TicaLogger);
                    Helpers.EscribirLogsVisiblesUsuario("Extracción de Aéreo manifiestos finalizado con éxito.");
                }
                #endregion
            }
            catch (Exception ex)
            {
                Logger.addLog("ID: "+ runID + ": "+ex.Message + " || Paso: Ejecución del programa");
                MessageBox.Show(ex.Message);
            }

        }

        #region Watchers de archivos Txt

        /// <summary>
        /// Crea el watch del archivo TICALogsUsuario.txt
        /// </summary>
        private void WatchTicaLogsUsuario() 
        {
            try
            {
                var watcher = new FileSystemWatcher(Path.GetTempPath());

                watcher.NotifyFilter = NotifyFilters.LastWrite;

                watcher.Changed += new FileSystemEventHandler(InformacionManifiestoAereoOnChanged);

                watcher.Filter = "TICALogsUsuario.txt";
                watcher.EnableRaisingEvents = true;
            }
            catch (Exception ex)
            {

                Logger.addLog(ex.Message + $" en método WatchTicaLogsUsuario()");
            }
        }

        #endregion

        #region Eventos para leer archivos TXT

        /// <summary>
        /// Este metodo se encarga de ver los cambios del archivo "InformacionManifiestoAereo.txt" para ir mostrando el en log de la aplicacion. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void InformacionManifiestoAereoOnChanged(object sender, FileSystemEventArgs e)
        {

            try
            {
                this.Dispatcher.Invoke(() =>
                {
                    Thread.Sleep(100);
                    using (var file = new StreamReader(e.FullPath))
                    {
                        var lineas = file.ReadToEnd().Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
                        newParagraph.Inlines.Add(new Run(lineas[lineas.Length-1]));
                        flowDocument.Blocks.Add(newParagraph);
                        newParagraph.Inlines.Add("\n");
                        flowDocument.Blocks.Add(newParagraph);
                        LoggerBox.Document = flowDocument;
                    }

                });
            }
            catch (Exception ex)
            {

                Logger.addLog(ex.Message + $" en método InformacionManifiestoAereoOnChanged()");
            }
        }
        #endregion

        #region Eventos
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            FlowDocument flowDocument = new FlowDocument();
            Paragraph newParagraph = new Paragraph();
            newParagraph.Inlines.Add(new Run("Starting app..."));
            flowDocument.Blocks.Add(newParagraph);
            LoggerBox.Document = flowDocument;
        }

        private void Detener(object sender, RoutedEventArgs e)
        {
            try
            {
                Process.Start("taskkill", "/f /im chromedriver.exe");
                Helpers.EscribirLogsVisiblesUsuario("El proceso ha sido cancelado");
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void EscribirLineaLog(string texto) 
        {
            newParagraph.Inlines.Add(new Run(texto));
            newParagraph.Inlines.Add("\n");
            flowDocument.Blocks.Add(newParagraph);
            LoggerBox.Document = flowDocument;
        }

        private void limpiarLog() 
        {
            TextRange textRange = new TextRange(LoggerBox.Document.ContentStart, LoggerBox.Document.ContentEnd);
            textRange.Text = String.Empty;
        }

        private void TipoCB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //Si la opcion de Viajes aereo está seleccionada, mostramos el checkbox del recinto de origen
            if (TipoCB.SelectedIndex == 1)
            {
                recintoOrigenCheckbox.Visibility = Visibility.Visible;
            }
            else
            {
                recintoOrigenCheckbox.Visibility = Visibility.Hidden;
            }
        }

        private void recintoOrigenCheckbox_Checked(object sender, RoutedEventArgs e)
        {
            recintoOrigenAereo = "P001";
        }

        private void recintoOrigenCheckbox_Unchecked(object sender, RoutedEventArgs e)
        {
            recintoOrigenAereo = string.Empty;
        }

        private void EliminaInformacionDB_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void EliminaInformacionDB_Unchecked(object sender, RoutedEventArgs e)
        {

        }

        public MessageBoxResult ConfirmaEliminacionInformacionExistente(string nombreReporte) 
        {
            MessageBoxResult confirmacion = MessageBox.Show("¿Está seguro(a) que desea eliminar la información existente del reporte " + nombreReporte + " de la base de datos?", "Confirmación", MessageBoxButton.OKCancel);
            return confirmacion;
        }

        private void DescargarArchivoResumenReporte(object sender, RoutedEventArgs e)
        {
            var archivo = Path.GetTempPath();
            if (TipoCB.SelectedIndex == 0)
            {
                archivo += "ResumenViajesMaritimo.txt";
            }
            if (TipoCB.SelectedIndex == 1)
            {
                archivo += "ResumenViajesAereo.txt";
            }
            if (TipoCB.SelectedIndex == 2)
            {
                archivo += "ResumenAereoManifiesto.txt";
            }
            var nombreArchivo = archivo.Replace(Path.GetTempPath(), "");

            if (!File.Exists(archivo))
            {
                MessageBox.Show($"El archivo {nombreArchivo} no ha sido generado");
                return;
            }
            SaveFileDialog saveFile = new SaveFileDialog();
            saveFile.DefaultExt = ".txt";
            saveFile.FileName = nombreArchivo;
            if (saveFile.ShowDialog()==true)
            {
                if (File.Exists(saveFile.FileName))
                {
                    File.Delete(saveFile.FileName);
                }
                File.Copy(archivo, saveFile.FileName);
            }
        }



        #endregion

        
    }
}
