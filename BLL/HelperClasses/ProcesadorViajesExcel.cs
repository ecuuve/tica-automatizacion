﻿using ExcelDataReader;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using BLL.Models;
using Nancy.Json;
using System.Linq;

namespace BLL
{
    public class ProcesadorViajesExcel
    {
        #region Properties
        public IWebDriver driver { get; set; }
        public WebDriverWait wait { get; set; }
        public string NombreArchivo { get; set; }
        public string TipoReporte { get; set; }   
        public string Ubicacion { get; set; }

        private string InsertarListaViajesMaritimo = "http://192.168.6.180:8080/devrenoir/tsmwww/AutoTICA/InsertarListaViajesMaritimo/";

        private string InsertarListaViajesAereo = "http://192.168.6.180:8080/devrenoir/tsmwww/AutoTICA/InsertarListaViajesAereo/";
        public List<RecintosMaritimo> listaRecintosMaritimo { get; set; }    
        #endregion
        public ProcesadorViajesExcel(IWebDriver driver, WebDriverWait wait)
        {
            this.driver = driver;
            this.wait = wait;
            this.NombreArchivo = NombreArchivo;
            this.TipoReporte = TipoReporte;
            this.Ubicacion = Path.GetTempPath() + "ReporteViajes";
        }

        /// <summary>
        /// Verifica la existencia de la carpeta que contiene el archivo de excel con los viajes descargados
        /// </summary>
        /// <param name="nombreArchivo">El nombre que se le va a asignar al archivo</param>
        /// <returns></returns>
        private bool EliminarArchivoExistente() 
        {
            try
            {
                DirectoryInfo di = new DirectoryInfo(Ubicacion);

                if (!Directory.Exists(Ubicacion))
                {
                    Directory.CreateDirectory(Ubicacion);
                }
                else
                {
                    var archivos = di.GetFiles();
                    foreach (var file in archivos)
                    {
                        file.Delete();
                    }
                }
                return true;    
            }
            catch (Exception ex)
            {
                Logger.addLog(ex.Message + $" en método EliminarArchivoExistente()");
                return false;
            }
        }       

        public bool DescargarArchivoExcel()
        {
            try
            {
                EliminarArchivoExistente();
                var btnReporte = wait.Until(e => e.FindElement(By.Name("BREPORTE")));
                btnReporte.Click();
                return VerificarDescargaExitosa();
            }
            catch (Exception ex)
            {
                Logger.addLog(ex.Message + $" en método DescargarArchivoExcel()");
                return false;
            }
        }

        private bool VerificarDescargaExitosa() 
        {
            bool archivoDescargado = false;
            try
            {
                while (!archivoDescargado)
                {
                    var listaArchivos = Directory.GetFiles(this.Ubicacion);
                    if (listaArchivos.Length>0)
                    {
                        archivoDescargado = true;
                        Thread.Sleep(1000);
                        //Se obtiene el nombre del archivo
                        this.NombreArchivo = Directory.GetFiles(this.Ubicacion)[0].ToString().Replace(Ubicacion+$"\\", "");
                        return true;
                    }   
                }
            }
            catch (Exception ex)
            {

                Logger.addLog(ex.Message+$" en método VerificarDescargaExitosa()");
            }
            return false;
        }

        public List<ViajeInformacionGeneral> LeerArchivo() 
        {
            List<ViajeInformacionGeneral> listaViajes = new List<ViajeInformacionGeneral>();
            try
            {
                System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);
                using (var stream = File.Open(Ubicacion+"\\"+NombreArchivo, FileMode.Open, FileAccess.Read))
                {
                    using (var reader = ExcelReaderFactory.CreateOpenXmlReader(stream))
                    {
                        var i = 0;
                        do
                        {
                            while (reader.Read())
                            {
                                if (i>0)
                                {
                                    var numeroViaje = reader.GetDouble(0).ToString();
                                    var aduanaOrigen = reader.GetString(3);
                                    var recintoDestino = reader.GetString(6).Trim();
                                    var fecha = reader.GetDateTime(7).ToString("dd/MM/yy", System.Globalization.CultureInfo.InvariantCulture).Replace(" 12:00:00 AM", "");
                                    ViajeInformacionGeneral viaje = new ViajeInformacionGeneral(numeroViaje, aduanaOrigen, recintoDestino, fecha);
                                    if (viaje!=null)
                                    {
                                        if (TipoReporte=="Maritimo")
                                        {
                                            var recintoValido = listaRecintosMaritimo.Where(x => x.rm_origen == recintoDestino).FirstOrDefault();
                                            
                                                if (recintoValido!=null)
                                                {
                                                    listaViajes.Add(viaje);
                                                }
                                        }
                                        else if (TipoReporte=="Aereo")
                                        {
                                            listaViajes.Add(viaje);
                                        }
                                    }
                                }
                                i++;
                                
                            }
                        } while (reader.NextResult());
                    }
                }
                List<ViajeInformacionGeneral> listaFiltrada = listaViajes.GroupBy(x => x.Viaje).Select(x => x.FirstOrDefault()).ToList();
                return listaFiltrada;
            }
            catch (Exception ex)
            {
                Logger.addLog(ex.Message + $" en método LeerArchivo()");
                return listaViajes;
            }
        }

        public bool GuardarListaViajes(List<ViajeInformacionGeneral> listaViajes) 
        {
            try
            {
                if (TipoReporte=="Maritimo")
                {
                    foreach (var viaje in listaViajes)
                    {

                        var json = new JavaScriptSerializer().Serialize(new
                        {
                            LVM_NUM_VIAJE_P = viaje.Viaje,
                            LVM_FECHA_P = viaje.Fecha,
                            LVM_ADUANA_ORIGEN_P = viaje.AduanaOrigen,
                            LVM_RECINTO_DESTINO_P = viaje.RecintoDestino
                        });

                        Helpers.LLamarWebService("POST", InsertarListaViajesMaritimo, json.ToUpper());
                    }
                }
                else if (TipoReporte == "Aereo")
                {
                    foreach (var viaje in listaViajes)
                    {

                        var json = new JavaScriptSerializer().Serialize(new
                        {
                            LVA_NUM_VIAJE_P = viaje.Viaje,
                            LVA_FECHA_P = viaje.Fecha
                        });
                        Helpers.LLamarWebService("POST", InsertarListaViajesAereo, json.ToUpper());
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                Logger.addLog(ex.Message + $" en método GuardarListaViajes()");
                return false;
            }
        }
    }
}
