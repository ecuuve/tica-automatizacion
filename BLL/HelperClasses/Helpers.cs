﻿using BLL.Models;
using Nancy.Json;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using System.Linq;
using BLL.Models.Aereo;

namespace BLL
{
    public class Helpers
    {

        public static IWebDriver LaunchDriver() 
        {
                       
            ChromeOptions chromeOptions = new ChromeOptions();
            chromeOptions.AddArgument("start-maximized");
            chromeOptions.AddArgument("enable-automation");
            chromeOptions.AddArgument("--headless");
            chromeOptions.AddArgument("--no-sandbox");
            chromeOptions.AddArgument("--disable-infobars");
            chromeOptions.AddArgument("--disable-extensions");
            chromeOptions.AddArgument("--disable-dev-shm-usage");
            chromeOptions.AddArgument("--disable-browser-side-navigation");
            chromeOptions.AddArgument("--disable-gpu");
            chromeOptions.AddArgument("--dns-prefetch-disable");
            chromeOptions.AddArgument("--restore-last-session");
            chromeOptions.AddArgument("--ui-test-action-max-timeout=10000");
            //Settings para descarga de archivos
            chromeOptions.AddUserProfilePreference("download.default_directory", Path.GetTempPath() + "ReporteViajes");
            chromeOptions.AddUserProfilePreference("disable-popup-blocking", "true");
            chromeOptions.AddUserProfilePreference("intl.accept_languages", "nl");
            string programFilesPath = Environment.ExpandEnvironmentVariables("%ProgramW6432%");
            IWebDriver driver = new ChromeDriver(programFilesPath + "\\ChromeDriver", chromeOptions);
            return driver;
        }
        public static void clearDriverCache(ChromeDriver driver)
        {
            driver.Manage().Cookies.DeleteAllCookies();
            //driver.ExecuteChromeCommand("clearBrowserData", );
            // you could also use                
            // driver.getDevTools().send(Network.clearBrowserCache());
        }

        public static void AgregarLineaDUA(string lineaDUA) 
        {
            string logPath = Path.GetTempPath() + "LineasDUA.txt";

            using (StreamWriter writer = new StreamWriter(logPath, true))
            {
                string linea = lineaDUA;

                writer.WriteLine(lineaDUA);

            }
        } 
        
        public static void AgregarLineaViajeAereo(string viajeAereo) 
        {
            string logPath = Path.GetTempPath() + "ViajeAereo.txt";

            using (StreamWriter writer = new StreamWriter(logPath, true))
            {
                string linea = viajeAereo;

                writer.WriteLine(viajeAereo);

            }
        }
        
        /// <summary>
        /// Utilizamos este metodo para loguear cada viaje. 
        /// </summary>
        /// <param name="informacionViaje"></param>
        public static void AgregarInformacionNumeroViaje(string informacionViaje) 
        {
            string logPath = Path.GetTempPath() + "InformacionViaje.txt";

            using (StreamWriter writer = new StreamWriter(logPath, true))
            {
                string linea = informacionViaje;

                writer.WriteLine(informacionViaje);

            }
        } 
        
        /// <summary>
        /// Guarda en un txt file el procesamiento del numero de manifiesto para el reporte aéreo. 
        /// </summary>
        /// <param name="informacionManifiesto"></param>
        public static void AgregarInformacionNumeroManifiestoAereo(string informacionManifiesto) 
        {
            string logPath = Path.GetTempPath() + "InformacionManifiestoAereo.txt";

            using (StreamWriter writer = new StreamWriter(logPath, true))
            {
                string linea = informacionManifiesto;

                writer.WriteLine(informacionManifiesto);

            }
        }

        /// <summary>
        /// Este archivo se encarga de escribir los logs que van a ser visibles para el usuario en el UI
        /// </summary>
        /// <param name="log"></param>
        public static void EscribirLogsVisiblesUsuario(string log)
        {
            string logPath = Path.GetTempPath() + "TICALogsUsuario.txt";
            try
            {
                using (StreamWriter writer = new StreamWriter(logPath, true))
                {

                    writer.WriteLine(log);

                }
            }
            catch (Exception ex)
            {

                Logger.addLog(ex.Message + $" en método EscribirLogsVisiblesUsuario()");
            }
        }

        /// <summary>
        /// Escribe el resumen de numero de viajes en un archivo txt 
        /// </summary>
        /// <param name="log"></param>
        public static void EscribirResumenNumeroViajes(string log, string tipoReporte)
        {
            string logPath = Path.GetTempPath() + $"Resumen{tipoReporte}.txt";

            using (StreamWriter writer = new StreamWriter(logPath, true))
            {
                writer.WriteLine(log);
            }
        }

        public static void EliminarResumenNumeroViajes(string tipoReporte) 
        {
            try
            {
                string archivo = Path.GetTempPath() + $"Resumen{tipoReporte}.txt";
                if (File.Exists(archivo))
                {
                    File.Delete(archivo);
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public static string LLamarWebService(string httpMethod, string url, string json) 
        {
            var response = string.Empty;
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(url);

                request.Method = httpMethod;

                //Si el metodo el tipo POST se envian parametros en Json, aunque los mismos vengan vacios. 
                if (httpMethod=="POST")
                {
                    if (json!=string.Empty)
                    {
                        request.ContentType = "application/json";
                        using (var streamWriter = new StreamWriter(request.GetRequestStream()))
                        {
                            streamWriter.Write(json.ToUpper());
                        }
                    }
                }

                var httpResponse = (HttpWebResponse)request.GetResponse();
                response = httpResponse.StatusCode.ToString();

                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                }

            }
            catch (Exception ex)
            {

                Logger.addLog(ex.Message + $"LLamarWebService()");
                return ex.Message;
            }

            return response;
        }

        public static List<RecintosMaritimo> ObtenerRespuestJsonWebService(string httpMethod, string url, string json)
        {
            var listaRecintos = new ListaRecintosMaritimo();
            //var jsonResult = new RecintosMaritimo();
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = httpMethod;
                var httpResponse = (HttpWebResponse)request.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                    var t = JObject.Parse(result);
                    t.Property("next").Remove();
                    listaRecintos = JsonConvert.DeserializeObject<ListaRecintosMaritimo>(t.ToString());
                    
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }

            return listaRecintos.recintosMaririmo;
        }

        public static List<ViajesMaritimosInformacion> ObtenerInformacionViajeMaritimoJsonWebService(string httpMethod, string url, string numeroViaje) 
        {
            var viaje = new ListaViajesMaritimosInformacion();
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(url+$"/{numeroViaje}");
                request.Method = httpMethod;
                var httpResponse = (HttpWebResponse)request.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                    var t = JObject.Parse(result);
                    t.Property("next").Remove();
                    viaje = JsonConvert.DeserializeObject<ListaViajesMaritimosInformacion>(t.ToString());
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
            return viaje.viajesMaritimosInformacion;
        }
        
        public static List<ManifiestosInformacion> ObtenerInformacionManifiestoAereoJsonWebService(string httpMethod, string url, string numeroManifiesto) 
        {
            var manifiesto = new ListaManifiestosInformacion();
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(url+$"/{numeroManifiesto}");
                request.Method = httpMethod;
                var httpResponse = (HttpWebResponse)request.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                    var t = JObject.Parse(result);
                    t.Property("next").Remove();
                    manifiesto = JsonConvert.DeserializeObject<ListaManifiestosInformacion>(t.ToString());
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
            return manifiesto.listaManifiestosInformacion;
        }

        public static List<ViajesAereosInformacion> ObtenerInformacionViajeAereoJsonWebService(string httpMethod, string url, string numeroViaje)
        {
            var viaje = new ListaViajesAereosInformacion();
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(url + $"/{numeroViaje}");
                request.Method = httpMethod;
                var httpResponse = (HttpWebResponse)request.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                    var t = JObject.Parse(result);
                    t.Property("next").Remove();
                    viaje = JsonConvert.DeserializeObject<ListaViajesAereosInformacion>(t.ToString());
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
            return viaje.viajesAereosInformacion;
        }

        //Trae el numero de guia, en caso de que ya exista 
        public static List<ManifiestosInformacion> ObtenerNumeroGuiaManifiestoJsonWebService(string httpMethod, string url, string numeroGuia)
        {
            var guia = new ListaManifiestosInformacion();
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(url + $"/{numeroGuia}");
                request.Method = httpMethod;
                var httpResponse = (HttpWebResponse)request.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                    var t = JObject.Parse(result);
                    if (result.Contains("next"))
                    {
                        t.Property("next").Remove();
                    }
                    guia = JsonConvert.DeserializeObject<ListaManifiestosInformacion>(t.ToString());
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
            return guia.listaManifiestosInformacion;
        }

        /// <summary>
        /// Se obtiene todos los records del web service recorriendo la paginacion de la respuesta
        /// </summary>
        /// <param name="httpMethod"></param>
        /// <param name="url"></param>
        /// <returns></returns>
        public static List<ViajesAereosInformacion> ObtenerInformacionViajeListaTotalAereoJsonWebServicePaginacion(string httpMethod, string url)
        {
            var viaje = new ListaViajesAereosInformacion();
            var numeroPagina = 0;
            var urlConPaginacion = url;
            var itemsDisponibles = true;
            List<ViajesAereosInformacion> lstViajes = new List<ViajesAereosInformacion>();
            try
            {
                while (itemsDisponibles)
                {
                    if (numeroPagina>0)
                    {
                        urlConPaginacion = url + $"?page={numeroPagina}";
                    }
                    else
                    {
                        urlConPaginacion = url;
                    }
                    var request = (HttpWebRequest)WebRequest.Create(urlConPaginacion);
                    request.Method = httpMethod;
                    var httpResponse = (HttpWebResponse)request.GetResponse();
                    using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                    {
                        var result = streamReader.ReadToEnd();
                        var t = JObject.Parse(result);
                        var nxtObj = t.Property("next");
                        if (nxtObj!=null)
                        {
                            t.Property("next").Remove();
                        }
                        viaje = JsonConvert.DeserializeObject<ListaViajesAereosInformacion>(t.ToString());
                        lstViajes.AddRange(viaje.viajesAereosInformacion);
                        if (nxtObj==null)
                        {
                            itemsDisponibles = false;
                        }
                    }
                    numeroPagina++; 
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
            return lstViajes;
        }

        public static List<ViajesMaritimosInformacion> ObtenerInformacionViajeListaTotalMaritimoJsonWebServicePaginacion(string httpMethod, string url)
        {
            var viajes = new ListaViajesMaritimosInformacion();
            var numeroPagina = 0;
            var urlConPaginacion = url;
            var itemsDisponibles = true;
            List<ViajesMaritimosInformacion> lstViajes = new List<ViajesMaritimosInformacion>();
            try
            {
                while (itemsDisponibles)
                {
                    if (numeroPagina > 0)
                    {
                        urlConPaginacion = url + $"?page={numeroPagina}";
                    }
                    else
                    {
                        urlConPaginacion = url;
                    }
                    var request = (HttpWebRequest)WebRequest.Create(urlConPaginacion);
                    request.Method = httpMethod;
                    var httpResponse = (HttpWebResponse)request.GetResponse();
                    using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                    {
                        var result = streamReader.ReadToEnd();
                        var t = JObject.Parse(result);
                        var nxtObj = t.Property("next");
                        if (nxtObj != null)
                        {
                            t.Property("next").Remove();
                        }
                        viajes = JsonConvert.DeserializeObject<ListaViajesMaritimosInformacion>(t.ToString());
                        lstViajes.AddRange(viajes.viajesMaritimosInformacion);
                        if (nxtObj == null)
                        {
                            itemsDisponibles = false;
                        }
                    }
                    numeroPagina++;
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
            return lstViajes;
        }

        /// <summary>
        /// Ejecuta un web service que contiene parametros de salida
        /// </summary>
        /// <param name="httpMethod"></param>
        /// <param name="url"></param>
        /// <param name="cantidadParametrosEsperados"></param>
        /// <returns></returns>
        public static Object ObtenerParametrosSalidaWebService(string httpMethod, string url, int cantidadParametrosEsperados) 
        {
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = httpMethod;
                var httpResponse = (HttpWebResponse)request.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                    var t = JObject.Parse(result);
                    return JsonConvert.DeserializeObject<ViajesJSON>(t.ToString());
                }
            }
            catch (Exception ex)
            {

                Logger.addLog(ex.Message + $" en método EscribirLogsVisiblesUsuario()");
                return ex;
            }
        }

        public static void CerrarProcesoChromeDriver() 
        {
            try
            {
                Process[] pname = Process.GetProcessesByName("chromedriver");
                //Si hay algun proceso corriendo de Chromedriver, se cierra 
                if (pname.Length>0)
                {
                    Process.Start("taskkill", "/f /im chromedriver.exe");
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public static IWebDriver ReiniciarBusqueda(IWebDriver driver, string tipoReporte) 
        {
            try
            {
                driver.Manage().Cookies.DeleteAllCookies();
                driver.Quit();
                driver = LaunchDriver();
                Thread.Sleep(2000);
                if (tipoReporte == "ViajesMaritimo" || tipoReporte == "ViajesAereo")
                {
                    driver.Navigate().GoToUrl("https://www.hacienda.go.cr/TICA/web/hvjentreadu.aspx");
                }
                if (tipoReporte=="ManifiestoAereo")
                {
                    driver.Navigate().GoToUrl("https://www.hacienda.go.cr/tica/web/hcicgmic.aspx");
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
            return driver;
        }

        /// <summary>
        /// Obtiene el numero de viajes por fecha en las busquedas para determinar cuantos viajes deberian procesarse por x fecha. 
        /// </summary>
        /// <param name="driver"></param>
        /// <param name="wait"></param>
        /// <returns></returns>
        public static Dictionary<DateTime, int> ObtenerCantidadViajesPorFecha(IWebDriver driver, WebDriverWait wait) 
        {
            Thread.Sleep(2000);
            Logic logic = new Logic(0, driver, wait);
            var listaFechas = new List<DateTime>();
            var listaFechasCantidad = new Dictionary<DateTime, int>();
            var hayOtraPagina = true;
            while (hayOtraPagina)
            {
                var informacionTablaViaje = wait.Until(e => e.FindElement(By.Id("Grid1ContainerTbl")));
                var tbody = wait.Until(c => informacionTablaViaje.FindElement(By.TagName("tbody")));
                var filas = wait.Until(c => tbody.FindElements(By.TagName("tr")));
                foreach (var fila in filas)
                {
                    var fch = fila.FindElement(By.XPath("td[10]")).FindElement(By.TagName("span")).Text;
                    DateTime fecha = DateTime.Parse(fch).Date;
                    listaFechas.Add(fecha);
                }

                var lista = listaFechas.GroupBy(x => x).Select(g => new { Value = g.Key, Count = g.Count() }).OrderBy(x => x.Value);
                hayOtraPagina = logic.SeleccionarSiguientePaginaTabla(1);
                listaFechasCantidad =  lista.ToDictionary(pair => pair.Value, pair=> pair.Count);
            }

            return listaFechasCantidad;
        }

        public static string MostrarInformacionCantidadPorFecha(Dictionary<DateTime, int> lista, string OrigenRecinto) 
        {
            string str = string.Empty;
            int total = 0;
            if (lista.Count==0)
            {
                str  += "No hay información adicional que mostrar" + Environment.NewLine;
            }
            else
            {
                str += $"Información sobre cantidad de viajes por fecha para {OrigenRecinto}: "+ Environment.NewLine;
                foreach (var item in lista)
                {
                    str += item.Key.ToString("dd/MM/yyyy") + " : " + item.Value + Environment.NewLine;
                    total += item.Value;
                }
                str += $"Total de viajes: {total}";
            }

            return str;
        }

        public static List<ViajesAereosInformacion> ObtenerListaViajesAereoFechasFormateadas(List<ViajesAereosInformacion> lista) 
        {
            List<ViajesAereosInformacion> viajes = new List<ViajesAereosInformacion>();  
            try
            {
                foreach (var viaje in lista)
                {
                    var fch = DateTime.Parse(viaje.lva_fecha);
                    var fchStr = fch.ToString("dd/MM/yy");
                    viaje.lva_fecha = fchStr;
                    viajes.Add(viaje);
                }
                return viajes;  
            }
            catch (Exception ex)
            {

                Logger.addLog(ex.Message + $"ObtenerListaFechasViajesAereo()");
                return null;
            }
        }

        public static List<ViajesMaritimosInformacion> ObtenerListaViajesMaritimoFechasFormateadas(List<ViajesMaritimosInformacion> lista)
        {
            List<ViajesMaritimosInformacion> viajes = new List<ViajesMaritimosInformacion>();
            try
            {
                foreach (var viaje in lista)
                {
                    var fch = DateTime.Parse(viaje.lvm_fecha);
                    var fchStr = fch.ToString("dd/MM/yy");
                    viaje.lvm_fecha = fchStr;
                    viajes.Add(viaje);
                }
                return viajes;
            }
            catch (Exception ex)
            {

                Logger.addLog(ex.Message + $"ObtenerListaViajesMaritimoFechasFormateadas()");
                return null;
            }
        }


    }
}
