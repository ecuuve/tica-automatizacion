﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using BLL.Models;
using System.Net;
using System.Net.Mail;
using OpenQA.Selenium;

namespace BLL
{
    public class Logger
    {
        public static void addLog(string message) 
        {
            string logPath = Path.GetTempPath()+"TICALogger.txt";

            using (StreamWriter writer = new StreamWriter(logPath, true))
            {
                string logMessage = DateTime.Now + " : " + message;

                writer.WriteLine(logMessage);
                
            }
        }

        /// <summary>
        /// We add an empty line to the file to identify the blocks of errors by instances when the application is run. 
        /// </summary>
        public static void addEmptyLineToFile() 
        {
            string logPath = Path.GetTempPath() + "TICALogger.txt";
            using (StreamWriter writer = new StreamWriter(logPath, true))
            {

                writer.WriteLine("------------------------------------");

            }
        }

        /// <summary>
        /// Elimina todos los log files creados cada vez que se inicia la aplicacion. 
        /// </summary>
        public static void EliminarLogFile(string fileName)
        {
            try
            {
                string file = Path.GetTempPath() + fileName;
                if (File.Exists(file))
                {
                    File.Delete(file);
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public static void EnviarLogsCorreo(string tipoReporte) 
        {
            try
            {
                var fromAddress = new MailAddress("cmedrano1490@gmail.com", "TICA App");
                var toAddress = new MailAddress("claudio.medrano@ecuuve.com", "ECUUVE");
                const string fromPassword = "egzqupiczoehdusj";
                string subject = "Archivo de logs para Aplicacion TICA";
                string body = "Este correo contiene información relacionada a los errores generados por la aplicación para el reporte de "+tipoReporte+". Este correo fue generado el "+DateTime.Now;
                var archivoAdjuntoPath = Path.GetTempPath() + Constantes.TicaLogger;



                var smtp = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
                };
                using (var message = new MailMessage(fromAddress, toAddress)
                {
                    Subject = subject,
                    Body = body

                })
                {
                    if (File.Exists(archivoAdjuntoPath))
                    {
                        FileStream fs = new FileStream(archivoAdjuntoPath, FileMode.Open, FileAccess.Read);
                        StreamReader s = new StreamReader(fs);
                        Attachment fileAttachment = new Attachment(fs, "LogFile"+tipoReporte.Replace(" ", "")+DateTime.Now+".txt");
                        message.Attachments.Add(fileAttachment);
                    }
                    smtp.Send(message);
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
