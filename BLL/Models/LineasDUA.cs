﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Models
{
    public class LineasDUA
    {
        public LineasDUA()
        {
            this.Item = Item;
            this.DescripcionComercial = DescripcionComercial;
            this.Bultos = Bultos;
            this.TipoBulto = TipoBulto;
            this.NombreConsignatario = NombreConsignatario;
            this.NumeroViaje = NumeroViaje;
            this.RazonSocial = RazonSocial;
        }
        public LineasDUA(string Item, string DescripcionComercial, int bultos, string tipoBulto, string nombreConsignatario, string NumeroViaje, string razonSocial)
        {
            this.Item = Item;
            this.DescripcionComercial = DescripcionComercial;
            this.Bultos = bultos;
            this.TipoBulto = tipoBulto;
            this.NombreConsignatario = nombreConsignatario;
            this.NumeroViaje = NumeroViaje;
            this.RazonSocial = razonSocial;
        }

        public string Item { set; get; }
        public string DescripcionComercial { set; get; }
        public int Bultos { set; get; }
        public string TipoBulto { set; get; }
        public string NombreConsignatario { set; get; }
        public string NumeroViaje { set; get; }
        public string RazonSocial { set; get; }
    }
}
