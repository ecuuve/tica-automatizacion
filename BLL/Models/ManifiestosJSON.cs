﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Models
{
    [JsonObject(MemberSerialization = MemberSerialization.OptIn)]
    public class ManifiestosJSON
    {
        [JsonProperty(PropertyName = "viaje_p")]
        public string viaje_p { get; set; }

    }
}
