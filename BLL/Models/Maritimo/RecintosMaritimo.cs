﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace BLL.Models
{
    [JsonObject(MemberSerialization = MemberSerialization.OptIn)]
    public class RecintosMaritimo
    {

        [JsonProperty(PropertyName = "rm_id")]
        public string rm_id { set; get; }
        [JsonProperty(PropertyName = "rm_origen")]
        public string rm_origen { set; get; }
        [JsonProperty(PropertyName = "rm_nombre")]
        public string rm_nombre { set; get; }
    }
}
