﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Models
{
    public class ListaViajesMaritimosInformacion
    {
        [JsonProperty("items")]
        public List<ViajesMaritimosInformacion> viajesMaritimosInformacion { get; set; }
    }
}
