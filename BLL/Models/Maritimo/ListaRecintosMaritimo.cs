﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Models
{
    public class ListaRecintosMaritimo
    {
        [JsonProperty("items")]
        public List<RecintosMaritimo> recintosMaririmo { set; get; }
    }
}
