﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Models
{
    [JsonObject(MemberSerialization = MemberSerialization.OptIn)]
    public class ViajesMaritimosInformacion
    {
        [JsonProperty(PropertyName = "lvm_id")]
        public string lvm_id { get; set;}

        [JsonProperty(PropertyName = "lvm_num_viaje")]
        public string lvm_num_viaje { get; set; }

        [JsonProperty(PropertyName = "lvm_fecha")]
        public string lvm_fecha { get; set; }

        [JsonProperty(PropertyName = "lvm_procesado")]
        public string lvm_procesado { get; set; }
        [JsonProperty(PropertyName = "lvm_aduana_origen")]
        public string lvm_aduana_origen { get; set; }

        [JsonProperty(PropertyName = "lvm_recinto_destino")]
        public string lvm_recinto_destino { get; set; }
    }
}
