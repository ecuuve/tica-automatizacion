﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Models
{
    public class MovimientoStock
    {
        public MovimientoStock()
        {
            this.RazonSocial = RazonSocial;
            this.Peso = Peso;
            this.Destino = Destino;
            this.NumeroMovimiento = NumeroMovimiento;
            this.TotalBultos = TotalBultos;
        }

        public MovimientoStock(string RazonSocial, double Peso, string Destino, string NumeroMovimiento, double TotalBultos)
        {
            this.RazonSocial = RazonSocial;
            this.Peso = Peso;
            this.Destino = Destino;
            this.NumeroMovimiento = NumeroMovimiento;
            this.TotalBultos = TotalBultos;
        }


        public string RazonSocial { set; get; }
        public double Peso { set; get; }
        public string Destino { set; get; }
        public string NumeroMovimiento { set; get; }
        public double TotalBultos { set; get; }
    }
}
