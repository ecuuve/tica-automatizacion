﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Models
{
    [JsonObject(MemberSerialization = MemberSerialization.OptIn)]
    public class ViajesJSON
    {
        [JsonProperty(PropertyName = "VIAJE_P")]
        public string viaje_p { get; set; } 
    }
}
