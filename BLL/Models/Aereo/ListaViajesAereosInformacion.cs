﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Models
{
    public class ListaViajesAereosInformacion
    {
        [JsonProperty("items")]
        public List<ViajesAereosInformacion> viajesAereosInformacion { get; set; }
    }
}
