﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Models.Aereo
{
    public class ManifiestosInformacion
    {
        [JsonProperty(PropertyName = "lm_id")]
        public string lm_id { get; set; }

        [JsonProperty(PropertyName = "lm_num_manifiesto")]
        public string lm_num_manifiesto { get; set; }
        
        [JsonProperty(PropertyName = "lm_fecha")]
        public string lm_fecha { get; set; }
        
        [JsonProperty(PropertyName = "lm_procesado")]
        public string lm_procesado { get; set; }

        [JsonProperty(PropertyName = "am_numero_guia")]
        public string am_numero_guia { get; set; }
    }
}
