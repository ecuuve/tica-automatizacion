﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Models
{
    public class ViajesAereo
    {
        public ViajesAereo() 
        {
            this.FechaCreacion = FechaCreacion;
            this.RecintoOrigen = RecintoOrigen;
            this.RecintoDestino = RecintoDestino;
            this.PesoOrigen = PesoOrigen;
            this.PesoDestino = PesoDestino;
        }

        public ViajesAereo(string FechaCreacion, string RecintoOrigen, string RecintoDestino, double PesoOrigen, double PesoDestino)
        {
            this.FechaCreacion = FechaCreacion;
            this.RecintoOrigen = RecintoOrigen;
            this.RecintoDestino = RecintoDestino;
            this.PesoOrigen = PesoOrigen;
            this.PesoDestino = PesoDestino;
        }

        public string FechaCreacion { set; get; }
        public string RecintoOrigen { set; get; }
        public string RecintoDestino { set; get; }
        public double PesoOrigen { set; get; }
        public double PesoDestino { set; get; }
    }
}
