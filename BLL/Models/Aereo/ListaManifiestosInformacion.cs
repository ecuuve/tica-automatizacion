﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Models.Aereo
{
    public class ListaManifiestosInformacion
    {
        [JsonProperty("items")]
        public List<ManifiestosInformacion> listaManifiestosInformacion { get; set; }
    }
}
