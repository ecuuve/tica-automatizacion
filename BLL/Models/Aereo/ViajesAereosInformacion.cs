﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Models
{
    public class ViajesAereosInformacion
    {
        [JsonProperty(PropertyName = "lva_id")]
        public string lva_id { get; set; }

        [JsonProperty(PropertyName = "lva_num_viaje")]
        public string lva_num_viaje { get; set; }
        
        [JsonProperty(PropertyName = "lva_fecha")]
        public string lva_fecha { get; set; }

        [JsonProperty(PropertyName = "lva_procesado")]
        public string lva_procesado { get; set; }
    }
}
