﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Models
{
    public class ViajeInformacionGeneral
    {

        public string Viaje { get; set; }
        public string AduanaOrigen { get; set; }
        public string RecintoDestino { get; set; }
        public string Fecha { get; set; }
        public ViajeInformacionGeneral(string viaje, string aduanaOrigen, string recintoDestino, string fecha)
        {
            Viaje = viaje;
            AduanaOrigen = aduanaOrigen;
            RecintoDestino = recintoDestino;
            Fecha = fecha;
        }


    }
}
