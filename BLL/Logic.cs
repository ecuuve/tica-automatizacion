﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using BLL.Models;
using System.Linq;
using System.Linq.Expressions;
using AngleSharp.Common;
using Nancy.Json;
using System.Net;

namespace BLL
{
    public class Logic
    {

        int runID;
        IWebDriver driver;
        WebDriverWait wait;

        public Logic(int runID, IWebDriver driver, WebDriverWait wait) 
        {
            this.runID = runID;
            this.driver = driver;
            this.wait = wait;
        }

        //Propiedades para Reporte maritimo 
        string recintoDestinoActual = string.Empty;
        string aduanaOrigenActual = string.Empty;
        string fechaInicioActual = string.Empty;
        string fechaFinalActual = string.Empty;
        /// <summary>
        /// Contiene la lista de razon social por cada linea
        /// </summary>
        Dictionary<string, string> listaRazonSocialEspecifica = new Dictionary<string, string>();
        /// <summary>
        /// Fecha de creacion que aparece en la columna con el mismo nombre para determinar la fecha de cada numero de viaje. 
        /// </summary>
        string fechaCreacionActualMaritimo = string.Empty; 

        //Propiedades para reporte Viajes Aereo 
        string recintoOrigenActual = string.Empty;
        /// <summary>
        /// Guarda los numeros de viaje que han sido procesados 
        /// </summary>
        List<string> numerosViajeMaritimo = new List<string>();

        //Propiedades para reporte Aereo Manifiesto
        string fechaAereoManifiesto = string.Empty;

        string tabViajesEntreAduanas = string.Empty; //Esta propiedad guarda el ID del tab de resultado de viajes (El principal para el reporte maritimo)
        string tabViajeCompleto = string.Empty; //Esta propiedad guarda el ID del tab del detalle de un viaje
        string tabDetalleDelDua = string.Empty; //Esta propiedad guarda el ID del tab de resultado de un numero de DUA y sus detalles
        string tabManifiestoStock = string.Empty; //Esta propiedad guarda el ID del tab de Manifiestos Asociados a DUA (Opcion de Manifiesto/Stock)
        string tabManifiestoIngresoCarga = string.Empty; //Esta propiedad guarda el ID del tab de Manifiestos Ingreso Carga (Donde se da click a la carpeta de "Conocim")

        string tabManifiestoAereo = string.Empty; //Esta propiedad guarda el ID del tab de resultado de viajes (El principal para el reporte aereo)
        string tabConocimientosEmbarque = string.Empty; //Esta propiedad guarda el ID del tab de resultado de dar click en "Conocimientos" cuando se busca manifiestos de ingreso de carga para aereo
        string tabHijosConocimientos = string.Empty; //Esta propiedad guarda el ID del tab de resultado de dar click en "Hijos" en el reporte de conocimientos para el aereo
        string tabLineasMercancia = string.Empty; //Esta propiedad guarda el ID del tab de resultado de dar click en "Lineas" en el reporte de conocimientos para el aereo
        string tabAfectacionesPorLinea = string.Empty; //Esta propiedad guarda el ID del tab de resultado de dar click en "Afectaciones" en el reporte de conocimientos para el aereo
        string tabMovimientoStock = string.Empty; //Esta propiedad guarda el ID del tab de resultado de dar click en el numero de movimeinto en el tab de "Afectaciones por linea"
        //Redestinos
        string tabMovimientoStockRedestino1 = string.Empty; //Esta propiedad guarda el ID del tab de resultado de dar click en el numero de movimiento cuando se va a ver los redestinos
        string tabMovimientoStockRedestino2 = string.Empty; //Esta propiedad guarda el ID del tab de resultado de dar click en el DET. de la tabla 
        string tabMovimientoStockRedestino3 = string.Empty; //Esta propiedad guarda el ID del tab de resultado de dar click en el DET. de la tabla 
        string tabMovimientoStockRedestino4 = string.Empty; //Esta propiedad guarda el ID del tab de resultado de dar click en "Ver Ancestros"
        string tabMovimientoStockRedestino5 = string.Empty; //Esta propiedad guarda el ID del tab de resultado de dar click en "Ver Ancestros"
        string tabMovimientoStockRedestino6 = string.Empty; //Esta propiedad guarda el ID del tab de resultado de dar click en "Ver Ancestros"
        string tabMovimientoStockRedestino7 = string.Empty; //Esta propiedad guarda el ID del tab de resultado de dar click en "Ver Ancestros"
        List<string> listaTabsRedestino = new List<string>(); //Lista de tabs para redestinos

        //Dexcripcion Anticipado Manifiesto
        List<string> listaTabsDescripcionAnticipados = new List<string>(); //Lista de tabs para descripcion anticipados
        string tabDescripcionAnticipado1 = string.Empty; 
        string tabDescripcionAnticipado2 = string.Empty;  
        string tabDescripcionAnticipado3 = string.Empty; 
        string tabDescripcionAnticipado4 = string.Empty; 

        //URL de WS 
        string insertaMaritimo = "http://192.168.6.180:8080/devrenoir/tsmwww/AutoTICA/InsertarLineasViajesMaritimo/";
        string insertaAereoViajes = "http://192.168.6.180:8080/devrenoir/tsmwww/AutoTICA/InsertarLineasViajesAereo/";
        string insertaAereoManifiestos = "http://192.168.6.180:8080/devrenoir/tsmwww/AutoTICA/InsertarLineasManifiestoAereo/";
        string ActualizarListaViajeMaritimo = "http://192.168.6.180:8080/devrenoir/tsmwww/AutoTICA/ActualizarListaViajeMaritimo/";
        string ActualizarListaViajeAereo = "http://192.168.6.180:8080/devrenoir/tsmwww/AutoTICA/ActualizarListaViajeAereo/";
        string ActualizarListaAereoManifiesto = "http://192.168.6.180:8080/devrenoir/tsmwww/AutoTICA/ActualizarListaAereoManifiesto/";
        string ObtenerViajeMaritimo = "http://192.168.6.180:8080/devrenoir/tsmwww/AutoTICA/ObtenerViajeMaritimo/";
        string ObtenerViajeAereo = "http://192.168.6.180:8080/devrenoir/tsmwww/AutoTICA/ObtenerViajeAereo/";
        string ObtenerManifiestoAereo = "http://192.168.6.180:8080/devrenoir/tsmwww/AutoTICA/ObtenerManifiestoAereo/";
        string ObtenerNumGuia = "http://192.168.6.180:8080/devrenoir/tsmwww/AutoTICA/ObtenerNumGuia/";

        string InsertarListaManifiestos = "http://192.168.6.180:8080/devrenoir/tsmwww/AutoTICA/InsertarListaManifiestos/";

        #region Maritimo

        //Realiza la busqueda de los viajes 
        /// <summary>
        /// 
        /// </summary>
        /// <param name="driver">Es el que contiene el URL actual</param>
        /// <param name="aduanaOrigen"></param>
        /// <param name="fechaInicio"></param>
        /// <param name="fechaFinal"></param>
        /// <param name="recintoDestino"></param>
        public void BuscarViajesMaritimo(string aduanaOrigen, string fechaInicio, string fechaFinal, string recintoDestino, bool esPrimeraBusqueda = true) 
        {
            try
            {
                recintoDestinoActual = recintoDestino;
                aduanaOrigenActual = aduanaOrigen;
                fechaInicioActual = fechaInicio;
                fechaFinalActual = fechaFinal;
                //Se ingresa el texto en los campos 
                if (esPrimeraBusqueda)
                {
                    Helpers.EscribirLogsVisiblesUsuario("Buscando viajes para Aduana Origen "+aduanaOrigen+" y Recinto "+ recintoDestino);
                }                
                var aduanaOrigenField = wait.Until(e=>e.FindElement(By.Id("vVADUORIG")));
                aduanaOrigenField.Click();
                aduanaOrigenField.Clear();

                var recintoDestinoField = wait.Until(e => e.FindElement(By.Id("vVRCTDST")));
                recintoDestinoField.Click();
                recintoDestinoField.Clear();

                //var fechaInicioField = wait.Until(e => e.FindElement(By.Id("vVFCHDESDE")));
                var fechaInicioField = wait.Until(e => e.FindElement(By.Id("vVFCHDESDE")));
                fechaInicioField.Click();
                fechaInicioField.Clear();
                var fechaFinalField = wait.Until(e => e.FindElement(By.Id("vVFCHHASTA")));
                //wait.Until(e => e.FindElement(By.Id("vVFCHHASTA"))).SendKeys(fechaFinal);
                fechaFinalField.Click();
                fechaFinalField.Clear();
                Thread.Sleep(1000);

                //Se realiza la busqueda
                var button = wait.Until(e => e.FindElement(By.Name("BUTTON1")));
                aduanaOrigenField.Click();
                aduanaOrigenField.SendKeys(aduanaOrigen);
                aduanaOrigenField.Clear();
                aduanaOrigenField.SendKeys(aduanaOrigen);
                recintoDestinoField.Click();
                recintoDestinoField.SendKeys(recintoDestino);
                fechaInicioField.Click();
                fechaInicioField.SendKeys(fechaInicio);
                fechaFinalField.Click();
                fechaFinalField.SendKeys(fechaFinal);
                Thread.Sleep(3000);
                button.Click();
                //wait.Until((c=>c. .ExpectedConditions.ElementToBeClickable(btnConfirmar));


            }
            catch (Exception ex)
            {
                Logger.addLog("ID: "+runID+": "+ex.Message + " || Paso: Busqueda de viajes");
            }
        }


        public IWebDriver ProcesarResultadosMaritimo()
        {
            try
            {
                Helpers.EscribirLogsVisiblesUsuario("Viajes encontrados con éxito.");
                var hayOtraPagina = true;
                var numeroPagina = 1;
                var numeroViajes = 0;
                BuscarViajesMaritimo(aduanaOrigenActual, fechaInicioActual, fechaFinalActual, recintoDestinoActual, false);
                while (hayOtraPagina)
                {
                    numeroViajes += RecorrerTablaViajesMaritimo(numeroPagina);
                    //Se reinicia el navegador para evitar que el navegador se para debido al error del timeout
                    driver = Helpers.ReiniciarBusqueda(driver, "ViajesMaritimo");
                    wait = new WebDriverWait(driver, TimeSpan.FromSeconds(30));
                    //Se realiza la busqueda de nuevo
                    //BuscarViajesMaritimo(aduanaOrigenActual, fechaInicioActual, fechaFinalActual, recintoDestinoActual, false);
                    hayOtraPagina = SeleccionarSiguientePaginaTabla(numeroPagina);
                    numeroPagina++;
                }
                Helpers.EscribirLogsVisiblesUsuario(numeroViajes+" viajes procesados exitosamente");
            }
            catch (Exception ex)
            {

                Logger.addLog("ID: " + runID + ": " + ex.Message + " || Paso: Procesamiento de resultados de viajes marítimo");
            }

            return driver;
        }

        public int RecorrerTablaViajesMaritimo(int numeroPagina) 
        {
            Thread.Sleep(3000);
            var contadorFilas = 0;
            try
            {
                tabViajesEntreAduanas = driver.CurrentWindowHandle;

                //var informacionTablaViaje = driver.FindElement(By.Id("Grid1ContainerTbl"));
                var informacionTablaViaje = wait.Until(e => e.FindElement(By.Id("Grid1ContainerTbl")));
                //var tbody = informacionTablaViaje.FindElement(By.TagName("tbody"));
                var tbody = wait.Until(c=> informacionTablaViaje.FindElement(By.TagName("tbody")));
                var filas = wait.Until(c => tbody.FindElements(By.TagName("tr")));
                var listaNumerosViaje = new List<string>();

                if (filas.Count==0)
                {
                    Helpers.EscribirLogsVisiblesUsuario("Ningún viaje encontrado para Aduana Origen "+aduanaOrigenActual+" y recinto "+recintoDestinoActual);
                }

                Helpers.AgregarInformacionNumeroViaje("Tipo de reporte: Marítimo:");
                foreach (var fila in filas)
                {
                    var columna = fila.FindElement(By.XPath("td[2]"));
                    //var numeroViaje = columna.FindElement(By.TagName("span")).Text;
                    var numeroViaje = columna.FindElement(By.TagName("span"));
                    var numeroEnTexto  = columna.FindElement(By.TagName("a")).Text;
                    //Se verifica si el viaje ya habia sido procesado anteriormente
                    var viajeYaProcesado = Helpers.ObtenerInformacionViajeMaritimoJsonWebService("GET", ObtenerViajeMaritimo, numeroEnTexto);
                    if (viajeYaProcesado!=null)
                    {
                        if (viajeYaProcesado.FirstOrDefault().lvm_procesado=="0")
                        {

                            fechaCreacionActualMaritimo = fila.FindElement(By.XPath("td[10]")).FindElement(By.TagName("span")).Text;
                            Helpers.AgregarInformacionNumeroViaje("Procesando viaje: "+numeroEnTexto+" | "+ (contadorFilas+1)+"/"+filas.Count+" de la página #"+numeroPagina+" | Fecha: "+fechaCreacionActualMaritimo);
                            AbrirNuevoTab(numeroViaje);
                            CambiarTab("Viajes Maritimo");
                            tabViajeCompleto = driver.CurrentWindowHandle;
                            AbrirNumeroDua();
                            AbrirLineasDelDua();
                            Helpers.AgregarLineaDUA("Procesando viaje #"+ numeroEnTexto + ":\n");
                            Helpers.EscribirLogsVisiblesUsuario("Procesando viaje " + numeroEnTexto+" de la página #"+numeroPagina+ " | Fecha: " + fechaCreacionActualMaritimo);
                            var listaLineas = ObtenerLineasDetalleDelDUA(numeroEnTexto);

                            //Si hay una sola linea, se categoriza como completo
                            if (listaLineas.Count==1)
                            {
                                EnviarInformacionWebServiceViajesMaritimo("Completo", listaLineas);
                            }

                            //Se verifica si las lineas contienen consignatarios unicos o si contiene multiples consignatarios
                            if (!ContieneConsignatariosUnico(listaLineas))
                            {
                                driver.Navigate().Back();
                                var listaRazonesSociales = VerificarManifiestoStock();
                                //Se limpia la lista de tabs de redestinos. 
                                listaTabsRedestino.Clear();

                                //Si las razones sociales son iguales, se categoriza con esa razon social 
                                var razonSocialFiltrado = listaRazonesSociales.Distinct();
                                //Si las razones sociales son las mismas, se categoriza por esa razon social
                                if (razonSocialFiltrado.Count()==1)
                                {
                                    EnviarInformacionWebServiceViajesMaritimo(razonSocialFiltrado.FirstOrDefault(), listaLineas);
                                }

                                //Si hay mas de una razon social, se procede a revisar cual es la que se repite mas veces o se suman los bultos en caso de que hayan la misma cantidad de cada una. 
                                if (razonSocialFiltrado.Count()>1)
                                {
                                    //De momento se va a categorizar por la primera que encontremos pero esto se debe cambiar para cumplir con el requerimiento
                                    EnviarInformacionWebServiceViajesMaritimo(razonSocialFiltrado.FirstOrDefault(), listaLineas);

                                    //var q = listaRazonesSociales.GroupBy(x => x).Select(g => new { Value = g.Key, Count = g.Count() }).OrderByDescending(x => x.Count);
                                    ////Si solo hay dos razones sociales y amb
                                    //if (true)
                                    //{

                                    //}
                                    //var t = q.FirstOrDefault();

                                }
                            }
                            cerrarTab();
                            //Limpiamos la lista de razones sociales individuales 
                            listaRazonSocialEspecifica.Clear();
                            contadorFilas++;
                        }
                        else if (viajeYaProcesado.FirstOrDefault().lvm_procesado == "1")
                        {
                            Helpers.EscribirLogsVisiblesUsuario($"El viaje {numeroEnTexto} ya habia sido procesado.");
                        }
                    }

                }

            }
            catch (Exception ex)
            {

                Logger.addLog("ID: " + runID + ": " + ex.Message + " || Paso: Obtencion de datos en tabla de viajes");
            }

            return contadorFilas;
        }

        /// <summary>
        /// Selecciona el boton de siguiente para pasar las paginas de la tabla y asi poder obtener todos los valores de la misma. 
        /// </summary>
        /// <param name="numeroPaginaDestino">Es la pagina a la que vamos a pasar</param>
        /// <returns></returns>
        public bool SeleccionarSiguientePaginaTabla(int numeroPaginaDestino) 
        {
            var cont = 0;
            try
            {
                Thread.Sleep(1000);
                var btnSiguiente = wait.Until(e => e.FindElement(By.ClassName("PagingButtonsNext")));
                var styleAttr = btnSiguiente.GetAttribute("style");
                //Si el style no contiene "cursor:default" quiere decir que hay mas paginas que consultar
                if (!styleAttr.Contains("cursor: default"))
                {
                    while(cont<numeroPaginaDestino)
                    {
                        Thread.Sleep(5000);
                        btnSiguiente = wait.Until(e => e.FindElement(By.ClassName("PagingButtonsNext")));
                        styleAttr = btnSiguiente.GetAttribute("style");
                        if (!styleAttr.Contains("cursor: default"))
                        {
                            btnSiguiente.Click();
                        }
                        else
                        {
                            return false;
                        }
                        cont++;
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {
                Logger.addLog("ID: " + runID + ": " + ex.Message + " || Paso: Selección de siguiente página.");
            }
            return false;
        }

        /// <summary>
        /// Toma un elemento web y abre un tab a partir del mismo. 
        /// </summary>
        /// <param name="driver"></param>
        /// <param name="link"></param>
        /// <returns></returns>
        private void AbrirNuevoTab(IWebElement webElement) 
        {
            try
            {
                Actions action = new Actions(driver);
                action.KeyDown(Keys.Control).MoveToElement(webElement).Click().Perform();
            }
            catch (Exception ex)
            {

                Logger.addLog("ID: " + runID + ": " + ex.Message + " || Paso: Abriendo nueva pestaña");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="reporte"></param>
        /// <param name="detalleExcepcion"></param> Este parametro se agrega para excepciones que se hagan a la hora de manejar tabs
        private void CambiarTab(string reporte, string detalleExcepcion = "") 
        {

            try
            {
                var originalWindow = driver.CurrentWindowHandle;
                //Loop through until we find a new window handle
                foreach (string window in driver.WindowHandles)
                {
                    if (reporte == "Viajes Maritimo" || reporte == "Viajes Aereo")
                    {

                        if (originalWindow == tabViajesEntreAduanas)
                        {
                            if (originalWindow != window)
                            {
                                wait.Until(e => e.SwitchTo().Window(window));
                                //driver.SwitchTo().Window(window);
                           
                                tabViajeCompleto = driver.CurrentWindowHandle;
                                break;
                            }
                        }
                        else if (window != tabViajesEntreAduanas && window != tabViajeCompleto && window!=tabManifiestoStock && window!= tabManifiestoIngresoCarga )
                        {
                            if (detalleExcepcion.Contains("redestinos")) 
                            {

                                if (detalleExcepcion == "redestinosTab1")
                                {
                                    wait.Until(e => e.SwitchTo().Window(window));
                                    tabMovimientoStockRedestino1 = driver.CurrentWindowHandle;
                                    listaTabsRedestino.Add(tabMovimientoStockRedestino1);
                                }
                                else if (detalleExcepcion== "redestinosTab2")
                                {
                                    wait.Until(e => e);
                                    wait.Until(e => e.SwitchTo().Window(window));
                                    tabMovimientoStockRedestino2 = driver.CurrentWindowHandle;
                                    listaTabsRedestino.Add(tabMovimientoStockRedestino2);
                                }
                                else if (detalleExcepcion == "redestinosTab3")
                                {
                                    wait.Until(e => e.SwitchTo().Window(window));
                                    tabMovimientoStockRedestino3 = driver.CurrentWindowHandle;
                                    listaTabsRedestino.Add(tabMovimientoStockRedestino3);
                                }
                                else if (detalleExcepcion == "redestinosTab4")
                                {
                                    wait.Until(e => e.SwitchTo().Window(window));
                                    tabMovimientoStockRedestino4 = driver.CurrentWindowHandle;
                                    listaTabsRedestino.Add(tabMovimientoStockRedestino4);
                                }
                                else if (detalleExcepcion == "redestinosTab5")
                                {
                                    wait.Until(e => e.SwitchTo().Window(window));
                                    tabMovimientoStockRedestino5 = driver.CurrentWindowHandle;
                                    listaTabsRedestino.Add(tabMovimientoStockRedestino5);
                                } 
                                else if (detalleExcepcion == "redestinosTab6")
                                {
                                    wait.Until(e => e.SwitchTo().Window(window));
                                    tabMovimientoStockRedestino6 = driver.CurrentWindowHandle;
                                    listaTabsRedestino.Add(tabMovimientoStockRedestino6);                                
                                }
                                else if (detalleExcepcion == "redestinosTab7")
                                {
                                    wait.Until(e => e.SwitchTo().Window(window));
                                    tabMovimientoStockRedestino7 = driver.CurrentWindowHandle;
                                    listaTabsRedestino.Add(tabMovimientoStockRedestino7);
                                }
                            }
                            else
                            {

                                driver.Close();
                                wait.Until(e => e.SwitchTo().Window(window));

                            }

                        }
                        //else if (window != tabViajesEntreAduanas && window != tabManifiestoStock && detalleExcepcion == "redestinos")
                        //{
                        //    wait.Until(e => e.SwitchTo().Window(window));
                        //    tabMovimientoStockRedestino1 = driver.CurrentWindowHandle;
                        //}
                    }

                    if (reporte=="Manifiestos Aereo")
                    {
                        if (originalWindow==tabManifiestoAereo)
                        {
                            if (originalWindow != window)
                            {
                                wait.Until(e => e.SwitchTo().Window(window));
                                //driver.SwitchTo().Window(window);

                                tabConocimientosEmbarque = driver.CurrentWindowHandle;
                                break;
                            }
                        }
                        //Si estamos en la ventana de conocimientos de embarque para reporte aereo y necesitamos ir a la ventana de "Hijos conocimientos" 
                        else if (originalWindow == tabConocimientosEmbarque && detalleExcepcion != "Guia HWB")
                        {
                            //Nos aseguramos que cambiamos al tab de "Hijos conocimientos"
                            if (originalWindow != window)
                            {
                                if (window!= tabManifiestoAereo)
                                {
                                    wait.Until(e => e.SwitchTo().Window(window));
                                    tabHijosConocimientos = driver.CurrentWindowHandle;
                                    break;
                                }
                            }
                        }
                        //Si estamos en el tab resultado de dar click en "Hijos"
                        //Aca se agrega una excepcion porque necesitamos acceder al tab de LineasMercancia desde otro link
                        //En este caso las guia HWB no necesitan ir a los hijos e ingresar al tab de Linea de Mercancias desde el tab de "HijosConocimientos"
                        else if (originalWindow==tabHijosConocimientos || detalleExcepcion=="Guia HWB")
                        {
                            //Nos aseguramos que cambiamos al tab de "Lineas de Mercancia"
                            if (originalWindow != window)
                            { 
                                if (window != tabManifiestoAereo && window != tabConocimientosEmbarque)
                                {
                                    wait.Until(e => e.SwitchTo().Window(window));
                                    tabLineasMercancia = driver.CurrentWindowHandle;
                                    break;
                                }   
                            
                            }
                        }

                        //Si estamos en el tab de lineas mercancia
                        else if (originalWindow == tabLineasMercancia)
                        {
                            //Nos aseguramos que cambiamos al tab de "Afectaciones por linea"
                            if (originalWindow != window)
                            {
                                if (window != tabManifiestoAereo && window != tabConocimientosEmbarque && window !=tabHijosConocimientos)
                                {
                                    wait.Until(e => e.SwitchTo().Window(window));
                                    tabAfectacionesPorLinea = driver.CurrentWindowHandle;
                                    break;
                                }

                            }
                        }

                        //Si estamos en el tab resultado de dar click en "Afectaciones"
                        else if (originalWindow==tabAfectacionesPorLinea)
                        {
                            //Nos aseguramos que cambiamos al tab de "Lineas de Mercancia"
                            if (originalWindow != window)
                            { 
                                if (window != tabManifiestoAereo && window != tabConocimientosEmbarque && window!= tabHijosConocimientos && window!= tabAfectacionesPorLinea && window!=tabLineasMercancia)
                                {
                                    wait.Until(e => e.SwitchTo().Window(window));
                                    tabMovimientoStock = driver.CurrentWindowHandle;
                                    break;
                                }   
                            
                            }
                        }
                        if (detalleExcepcion.Contains("DescripcionAnticipados"))
                        {
                            if (window != tabManifiestoAereo && window != tabConocimientosEmbarque && window != tabHijosConocimientos && window != tabAfectacionesPorLinea)
                            {
                                if (detalleExcepcion == "DescripcionAnticipadosTab1")
                                {
                                    wait.Until(e => e.SwitchTo().Window(window));
                                    tabDescripcionAnticipado1 = driver.CurrentWindowHandle;
                                    listaTabsDescripcionAnticipados.Add(tabDescripcionAnticipado1);
                                }
                                else if(detalleExcepcion == "DescripcionAnticipadosTab2")
                                {
                                    wait.Until(e => e.SwitchTo().Window(window));
                                    tabDescripcionAnticipado2 = driver.CurrentWindowHandle;
                                    listaTabsDescripcionAnticipados.Add(tabDescripcionAnticipado2);
                                }
                                else if (detalleExcepcion == "DescripcionAnticipadosTab3")
                                {
                                    wait.Until(e => e.SwitchTo().Window(window));
                                    tabDescripcionAnticipado3 = driver.CurrentWindowHandle;
                                    listaTabsDescripcionAnticipados.Add(tabDescripcionAnticipado3);
                                }
                            }
                        }
                    }


                }
            }
            catch (Exception ex)
            {

                Logger.addLog("ID: " + runID + ": " + ex.Message + " || Paso: Cambiando a nueva pestaña abierta");
            }
        }


        private void cerrarTab() 
        {
            try
            {
                var parentTab = wait.Until(e => e.WindowHandles[0]);
                driver.Close();
                //var parentTab = driver.WindowHandles[0];
                //driver.SwitchTo().Window(parentTab);
                wait.Until(e => e.SwitchTo().Window(parentTab));
            }
            catch (Exception ex)
            {

                Logger.addLog("ID: " + runID + ": " + ex.Message + " || Paso: Cerrando un tab");
            }
        }
        /// <summary>
        /// Selecciona el link de lineas del DUA
        /// </summary>
        private void AbrirNumeroDua() 
        {
            try
            {
                //var btn = wait.Until(ExpectedConditions.ElementExists(By.Id("span_NUME_CORRE_0002")));
                var btn = wait.Until(e => e.FindElement(By.Id("span_NUME_CORRE_0002")));
                btn.Click();
                CambiarTab("Viajes Maritimo");
            }
            catch (Exception ex)
            {

                Logger.addLog("ID: " + runID + ": " + ex.Message + " || Paso: Abriendo número de DUA");
            }
        }

        /// <summary>
        /// Selecciona el boton de 'Lineas del DUA' para abrir dicha pagina con los detalles
        /// </summary>
        private void AbrirLineasDelDua() 
        {
            try
            {
                var element = wait.Until(e=>e.FindElement(By.XPath("//input[@value='Lineas del DUA']")));
                element.Click();
            }
            catch (Exception ex)
            {

                Logger.addLog("ID: " + runID + ": " + ex.Message + " || Paso: Abriendo 'Lineas del DUA'");
            }
        }

        /// <summary>
        /// Se obtienen todas las lineas del detalle de un DUA
        /// </summary>
        private List<LineasDUA> ObtenerLineasDetalleDelDUA(string numeroViaje) 
        {
            List<LineasDUA> listaLineasDUA = new List<LineasDUA>();
            try
            {
                //var lineas = wait.Until(ExpectedConditions.ElementExists(By.Id("SeriesContainerTbl"))).FindElement(By.TagName("tbody")).FindElements(By.TagName("tr"));
                var tabla= wait.Until(e => e.FindElement(By.Id("SeriesContainerTbl")));
                //var body = wait.Until(e => e.FindElement(By.TagName("tbody")));
                var body = tabla.FindElement(By.TagName("tbody"));
                //var lineas = wait.Until(e => e.FindElements(By.TagName("tr")));
                var lineas = body.FindElements(By.TagName("tr"));
                //var lineas = wait.Until(c => cuerpoTablaLineasDUA.FindElements(By.TagName("tr")));


                var index = 1;
                foreach (var linea in lineas)
                {
                    string numero = string.Empty;
                    if (index < 10)
                    {
                        numero = "000" + index;
                    }
                    else if (index>=10 && index < 100)
                    {
                        numero = "00" + index;
                    }
                    else if (index >=1000)
                    {
                        numero = "0" + index;
                    }
                    var Item = linea.FindElement(By.Id("span_vNUME_SERIE_" + numero)).Text;
                    var descripcionComercial = linea.FindElement(By.Id("span_vDESC_COMER_"+numero)).Text;
                    var bultos = int.Parse(linea.FindElement(By.Id("span_vVCANTBULTO_"+numero)).Text);
                    var tipoBulto = linea.FindElement(By.Id("span_vVTIPBDSC_"+numero)).Text;
                    var nombreConsignatario = linea.FindElement(By.Id("span_vDNOMDEST_"+numero)).Text;
                    var razonSocial = string.Empty;

                    listaLineasDUA.Add(new LineasDUA(Item, descripcionComercial, bultos, tipoBulto, nombreConsignatario, numeroViaje, razonSocial));
                    index++;

                    Helpers.AgregarLineaDUA("("+index+") ->"+ descripcionComercial +"--"+ bultos +"--"+ tipoBulto+"--"+nombreConsignatario);
                }

                var numeroLineas = lineas.Count;
                //cerrarTab();
            }
            catch (Exception ex)
            {

                Logger.addLog("ID: " + runID + ": " + ex.Message + " || Paso: Obteniendo detalles de Lineas del DUA");
            }

            return listaLineasDUA;
        }

        /// <summary>
        /// Obtiene la cantidad de lineas de DUA de un número de DUA
        /// </summary>
        /// <returns>Cantidad de numeros de DUA</returns>
        private int ObtenerCantidadLineasDUA() 
        {
            try
            {
                return int.Parse(wait.Until(e => e.FindElement(By.Id("span_vVLINEAS")).Text));
            }
            catch (Exception ex)
            {

                Logger.addLog("ID: " + runID + ": " + ex.Message + " || Paso: Obtención de cantidad de lineas de DUA");
            }

            return 0;
        }

        /// <summary>
        /// Verifica si los consignatarios de la lista de lineas son iguales o no. 
        /// </summary>
        /// <returns></returns>
        private bool ContieneConsignatariosUnico(List<LineasDUA> lineas)
        {
            //Se obtiene lista de consignatarios
            var consignatarios = lineas.Select(l=>l.NombreConsignatario).ToList().Distinct();
            //En caso de que solo sean dos consignatarios, los comparamos
            if (consignatarios.Count()==2)
            {
                if (consignatarios.First() != consignatarios.Last())
                {
                    return false;
                }
            }

            if (consignatarios.Count()!=lineas.Count)
            {
                return false;
            }

            if (consignatarios.Count()>2 && consignatarios.Count() == lineas.Count)
            {
                return false;
            }
            return true;
        }


        /// <summary>
        /// Obtiene la lista de numeros de conocimientos que se necesita para verificar en el campo de Secuencia TICA
        /// </summary>
        /// <returns></returns>
        private Dictionary<string, string> ObtenerListaConocimientos() 
        {
            var listaNumeroConocimiento = new Dictionary<string, string>();
            try
            {
                var index = 1;
                var tabla = wait.Until(e => e.FindElement(By.Id("SubmanifContainerTbl")));
                var body = tabla.FindElement(By.TagName("tbody"));
                var lineas = body.FindElements(By.TagName("tr"));

                foreach (var linea in lineas)
                {
                    string numero = string.Empty;
                    if (index < 10)
                    {
                        numero = "000" + index;
                    }
                    else if (index >= 10 && index < 100)
                    {
                        numero = "00" + index;
                    }
                    else if (index >= 1000)
                    {
                        numero = "0" + index;
                    }
                    //var fila = wait.Until(e=>e.FindElement(By.Id("SubmanifContainerRow_"+numero)));
                    //var columna = wait.Until(e=>fila.FindElement(By.XPath("//td[@colindex='7']")));
                    var valorConocimiento = wait.Until(e=>e.FindElement(By.Id("span_CGNROCONDNA_"+numero)).Text);
                    listaNumeroConocimiento.Add(numero, valorConocimiento);
                    index++;
                }
            }
            catch (Exception ex)
            {

                Logger.addLog("ID: " + runID + ": " + ex.Message + " || Paso: Obteniendo numeros de conocimiento para Secuencia TICA");
            }

            return listaNumeroConocimiento;
        }


        /// <summary>
        /// Obtiene una lista de razones sociales que nos va a permitir categorizar las lineas 
        /// </summary>
        /// <returns></returns>
        private List<string> VerificarManifiestoStock(bool EsRedestino = false) 
        {
            try
            {
                if (!EsRedestino)
                {
                    var btn = wait.Until(e=>e.FindElement(By.XPath("//input[@value='Manifiesto/Stock']")));
                    btn.Click();
                    //Se asigna el ID al tab de Manifiesto/Stock
                    tabManifiestoStock = driver.CurrentWindowHandle;
                }
                var listaConocimientos = ObtenerListaConocimientos();

                //Si hay conocimientos se sigue el proceso de verificacion de secuencia TICA
                if (listaConocimientos.Count>0)
                {
                    //Se obtiene el link del manifiesto
                    var link = wait.Until(e=>e.FindElement(By.Id("span_CGNROMIC_0001"))).FindElement(By.TagName("a"));
                    link.Click();
                    //Hacemos que el driver cambie de pestaña
                    CambiarTab("Viajes Maritimo");

                    tabManifiestoIngresoCarga = driver.CurrentWindowHandle;
                    //Se obtiene el link para ingresar a los conocimientos
                    var linkConocimientos = wait.Until(e => e.FindElement(By.Id("Grid1ContainerRow_0001"))).
                    FindElement(By.XPath("//td[@colindex='0']")).FindElement(By.TagName("a"));
                    linkConocimientos.Click();
                    CambiarTab("Viajes Maritimo");

                    //Devuelve la lista de razones sociales para categorizar las lineas 
                    return BuscarSecuenciaTICA(listaConocimientos);
                }
                else //En otro caso, se toma la informacion que se encuentra en "Observaciones" para tomar los detalles del mismo 
                {
                    return ObtenerRedestinos();
                }


            }
            catch (Exception ex)
            {

                Logger.addLog("ID: " + runID + ": " + ex.Message + " || Paso: Verificando manifiesto Stock");
            }

            //Se devuelve un objeto vacio si no hay razones sociales que revisar
            return new List<string>();
        }

        public List<string> ObtenerRedestinos() 
        {
            var listaNumeroConocimiento = new Dictionary<string, string>();
            try
            {                
                var tabla = wait.Until(e => e.FindElement(By.Id("SubstkContainerTbl")));
                var body = tabla.FindElement(By.TagName("tbody"));
                var lineas = body.FindElements(By.TagName("tr"));
                if (lineas.Count>0)
                {
                    IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
                    //js.ExecuteScript("document.getElementById('span_CGMOVSKID_0001').getElementsByTagName('a').setAttribute('target', 'self');");
                    var t = js.ExecuteScript("document.getElementById('span_CGMOVSKID_0001').getElementsByTagName('a')");
                    //js.ExecuteScript("document.getElementsByTagName('a').target='_self'");
                    var numMovimiento = wait.Until(e => e.FindElement(By.Id("span_CGMOVSKID_0001"))).FindElement(By.TagName("a"));
                    numMovimiento.Click();
                    CambiarTab("Viajes Maritimo", "redestinosTab1");
                    //Se busca folder llamado "DET." 
                    var fila = wait.Until(e => e.FindElement(By.Id("Grid1ContainerRow_0001")));
                    var det1 = fila.FindElement(By.XPath("td[5]")).FindElement(By.TagName("a"));
                    det1.Click();
                    CambiarTab("Viajes Maritimo", "redestinosTab2");
                    //Ver ancestros
                    var ancestros  = wait.Until(e => e.FindElement(By.Id("span_vVTITULO1"))).FindElement(By.TagName("a"));
                    ancestros.Click();
                    CambiarTab("Viajes Maritimo", "redestinosTab3");
                    //Se da click en el numero de movimiento #2
                    var numMovimiento2 = wait.Until(e => e.FindElement(By.Id("span_CGMOVSKID_0001"))).FindElement(By.TagName("a"));
                    numMovimiento2.Click();
                    CambiarTab("Viajes Maritimo", "redestinosTab4");
                    fila = wait.Until(e => e.FindElement(By.Id("Grid1ContainerRow_0001")));
                    var det2 = fila.FindElement(By.XPath("td[5]")).FindElement(By.TagName("a"));
                    det2.Click();
                    CambiarTab("Viajes Maritimo", "redestinosTab5");
                    var numeroDua = wait.Until(e => e.FindElement(By.Id("span_vVVALOR3"))).FindElement(By.TagName("a"));
                    numeroDua.Click();
                    CambiarTab("Viajes Maritimo", "redestinosTab6");
                    var btn = wait.Until(e => e.FindElement(By.XPath("//input[@value='Manifiesto/Stock']")));
                    btn.Click();
                    CambiarTab("Viajes Maritimo", "redestinosTab7");
                    CerrarTabsRedestino();
                    var listaRazonesSociales = VerificarManifiestoStock(true);
                    driver.Close();
                    wait.Until(e => e.SwitchTo().Window(tabManifiestoStock));
                    //driver.Close();
                    return listaRazonesSociales;
                }
            }
            catch (Exception ex)
            {

                Logger.addLog("ID: " + runID + ": " + ex.Message + " || Paso: Obteniendo numeros de conocimiento para Secuencia TICA");
            }
            return new List<string>();
        }

        public bool CerrarTabsRedestino() 
        {
            try
            {
                foreach (var tab in listaTabsRedestino.Distinct())
                {
                    if (tab!= tabMovimientoStockRedestino7)
                    {
                        wait.Until(e => e.SwitchTo().Window(tab));
                        driver.Close();
                    }
                }

                wait.Until(e => e.SwitchTo().Window(tabMovimientoStockRedestino7));
                return true;

            }
            catch (Exception ex)
            {
                Logger.addLog("ID: " + runID + ": " + ex.Message + " en CerrarTabsRedestino()");
                return false;
            }
        }

        /// <summary>
        /// Se encarga de buscar las secuencias TICA que recibe para extraer la razon social. 
        /// </summary>
        /// <param name="listaConocimientos"></param>
        /// <returns></returns>
        public List<string> BuscarSecuenciaTICA(Dictionary<string, string> listaConocimientos) 
        {

            List<string> listaRazonSocial = new List<string>();
            try
            {
                var secuenciaTicaInput = wait.Until(e=>e.FindElement(By.Id("vCGNROCONDNA")));
                var confirmarBtn = wait.Until(e=>e.FindElement(By.Name("BTN_ENTER1")));
                secuenciaTicaInput.Click();
                secuenciaTicaInput.Clear();

                foreach (var conocimiento in listaConocimientos)
                {
                    //secuenciaTicaInput.Click();
                    //secuenciaTicaInput.Clear();
                    secuenciaTicaInput.Click();
                    Thread.Sleep(1000);
                    secuenciaTicaInput.SendKeys(conocimiento.Value);
                    secuenciaTicaInput.Clear();
                    secuenciaTicaInput.SendKeys(conocimiento.Value);
                    Thread.Sleep(1000);
                    confirmarBtn.Click();
                    Thread.Sleep(2000);
                    //Obtenemos la razon social y la guardamos en la lista que vamos a devolver
                    var fila = wait.Until(e => e.FindElement(By.Id("Grid1ContainerRow_0001")));
                    var razonSocial = wait.Until(e=>fila.FindElement(By.XPath("td[9]")).FindElement(By.TagName("span"))).Text;
                    listaRazonSocial.Add(razonSocial);
                    //Agregamos la razon social junto con el item para identificar la razon social individual de cada uno
                    listaRazonSocialEspecifica.Add(conocimiento.Key, razonSocial);
                }
            }
            catch (Exception ex)
            {

                Logger.addLog("ID: " + runID + ": " + ex.Message + " || Paso: Buscar Secuencia TICA");
            }

            return listaRazonSocial;
        }


        /// <summary>
        /// Envia la informacion y categoriza la o las lineas de DUA
        /// </summary>
        /// <param name="categorizacion"></param>
        /// <param name="lineasDUA"></param>
        public void EnviarInformacionWebServiceViajesMaritimo(string categorizacion, List<LineasDUA> lineasDUA) 
        {
            var numeroViaje = string.Empty;
            try
            {
                foreach (var item in lineasDUA)
                {
                    var razonSocial = string.Empty;
                    if (listaRazonSocialEspecifica.Count > 0)
                    {
                        var Item = item.Item;
                        razonSocial = listaRazonSocialEspecifica.Where(r => r.Key == Item).FirstOrDefault().Value;
                    }
                    //Se crea el JSON que se envia al WS
                    var json = new JavaScriptSerializer().Serialize(new
                    {
                        MAR_BULTOS_P = item.Bultos,
                        MAR_CATEGORIZACION_P = categorizacion,
                        MAR_DESC_COMERCIAL_P = item.DescripcionComercial,
                        MAR_NMB_CONSIGNATARIO_P = item.NombreConsignatario,
                        MAR_NUM_VIAJE_P = item.NumeroViaje,
                        MAR_RECINTO_DEST_P = recintoDestinoActual,
                        MAR_TIPO_BULTO_P = item.TipoBulto, 
                        MAR_ADUANA_ORIGEN_P = aduanaOrigenActual,
                        MAR_ITEM_P = item.Item,
                        MAR_RAZON_SOCIAL_P = razonSocial,
                        MAR_FCH_CREACION_P = fechaCreacionActualMaritimo
                    });
                    numeroViaje = item.NumeroViaje;
                    var respuesta = Helpers.LLamarWebService("POST", insertaMaritimo, json);
                    //Si el viaje se ingreso con exito, se actualiza el mismo en la tabla de lista de viajes maritimo. 
                    if (respuesta==HttpStatusCode.OK.ToString())
                    {
                         json = new JavaScriptSerializer().Serialize(new
                        {
                             NUM_VIAJE_P = item.NumeroViaje,
                             PROCESADO_P = "1"
                        });
                        Helpers.LLamarWebService("POST", ActualizarListaViajeMaritimo, json);
                    }
                    else if (respuesta.Contains("500"))
                    {
                        Helpers.EscribirLogsVisiblesUsuario($"Error al guardar el viaje {item.NumeroViaje}");
                    }
                }

            }
            catch (Exception ex)
            {

                Logger.addLog("ID: " + runID + ": " + ex.Message + " || Paso: Guarda informacion al Web Service para el reporte maritimo para número de viaje "+numeroViaje);
            }
        }

        #endregion

        #region Aereo

        #region Reporte Aereo Viajes

        public void BuscarViajesAereo(string fechaInicio, string fechaFinal, string recintoOrigen, bool esPrimeraBusqueda = true)
        {
            try
            {
                recintoOrigenActual = recintoOrigen;
                fechaInicioActual = fechaInicio;
                fechaFinalActual = fechaFinal;
                if (esPrimeraBusqueda)
                {
                    Helpers.EscribirLogsVisiblesUsuario("Buscando viajes para reporte aéreo...");
                }
                //Se ingresa el texto en los campos 
                var aduanaOrigenField = wait.Until(e => e.FindElement(By.Id("vVADUORIG")));
                aduanaOrigenField.Click();
                aduanaOrigenField.Clear();
                //Aduana Destino
                var aduanaDestinoField = wait.Until(e => e.FindElement(By.Id("vVADUDST")));
                aduanaDestinoField.Click();
                aduanaDestinoField.Clear();
                //Recinto Origen
                var recintoOrigenField = wait.Until(e => e.FindElement(By.Id("vVRCTORIG")));
                recintoOrigenField.Click();
                recintoOrigenField.Clear();
                //Fecha inicio
                var fechaInicioField = wait.Until(e => e.FindElement(By.Id("vVFCHDESDE")));
                fechaInicioField.Click();
                fechaInicioField.Clear();
                //Fecha final
                var fechaFinalField = wait.Until(e => e.FindElement(By.Id("vVFCHHASTA")));
                fechaFinalField.Click();
                fechaFinalField.Clear();
                //Estado del viaje
                var estadoViaje = wait.Until(e=>e.FindElement(By.Id("vVESTVJ")));
                var selectElement = new SelectElement(estadoViaje);
                selectElement.SelectByValue("COM");



                var button = wait.Until(e => e.FindElement(By.Name("BUTTON1")));
                //Se ingresa aduana origen y aduana destino 
                aduanaOrigenField.Click();
                aduanaOrigenField.Click();
                Thread.Sleep(1000);
                aduanaOrigenField.Click();
                aduanaOrigenField.SendKeys("005");
                Thread.Sleep(1000);
                aduanaDestinoField.Click();
                aduanaDestinoField.SendKeys("005");
                //Se ingresa el recinto origen 
                recintoOrigenField.SendKeys(recintoOrigen);
                //Fechas
                fechaInicioField.Click();
                fechaInicioField.SendKeys(fechaInicio);
                fechaFinalField.Click();
                fechaFinalField.SendKeys(fechaFinal);
                Thread.Sleep(3000);
                //Se realiza la busqueda
                button.Click();


            }
            catch (Exception ex)
            {
                Logger.addLog("ID: " + runID + ": " + ex.Message + " || Paso: Busqueda de viajes Aéreo");
            }
        }

        public IWebDriver ProcesarResultadosAereo(List<ViajesAereosInformacion> listaViajes)
        {
            try
            {
                var cantidadViajes = 0;
                var listaViajesFchFormateada = Helpers.ObtenerListaViajesAereoFechasFormateadas(listaViajes);
                var lstFechas = listaViajesFchFormateada.GroupBy(x=>x.lva_fecha).Distinct().ToList();
                foreach (var fch in lstFechas)
                {
                    Thread.Sleep(2000);
                    var hayOtraPagina = true;
                    var numeroPagina = 1;
                    //La cantidad de viajes que se procesan en total en todas las paginas recorridas
                    //Se obtiene la cantidad de fechas a ser procesadas
                    BuscarViajesAereo(fch.Key, fch.Key, recintoOrigenActual, false);
                    Thread.Sleep(3000);
                    //Si la fecha que se va a buscar ya tiene todos los viajes procesados, no se realiza el proceso de nuevo ya que no es necesario. 
                    var viajesPendientes = listaViajesFchFormateada.Where(f => f.lva_fecha == fch.Key && f.lva_procesado=="0").ToList();
                    if (viajesPendientes.Count>0)
                    {
                        while (hayOtraPagina)
                        {
                            cantidadViajes+= RecorrerTablaViajesAereo(numeroPagina);
                            wait = new WebDriverWait(driver, TimeSpan.FromSeconds(30));
                            hayOtraPagina = SeleccionarSiguientePaginaTabla(numeroPagina);
                            numeroPagina++;
                            Thread.Sleep(5000);
                        }
                    }
                    else
                    {
                        Helpers.EscribirLogsVisiblesUsuario($"Todos los viajes para la fecha {fch.Key} han sido procesados anteriormente");
                    }
                }

                Helpers.EscribirLogsVisiblesUsuario("Se procesaron " + cantidadViajes + " para el reporte de viajes aéreo.");
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return driver;
        }

        public int RecorrerTablaViajesAereo(int numeroPagina)
        {
            Thread.Sleep(3000);
            var contadorFilas = 0;
            var numViaje = string.Empty;
            try
            {
                tabViajesEntreAduanas = driver.CurrentWindowHandle;

                //var informacionTablaViaje = driver.FindElement(By.Id("Grid1ContainerTbl"));
                var informacionTablaViaje = wait.Until(e => e.FindElement(By.Id("Grid1ContainerTbl")));
                //var tbody = informacionTablaViaje.FindElement(By.TagName("tbody"));
                var tbody = wait.Until(c => informacionTablaViaje.FindElement(By.TagName("tbody")));
                var filas = wait.Until(c => tbody.FindElements(By.TagName("tr")));
                if (!filas.FirstOrDefault().Size.IsEmpty)
                {
                    Helpers.EscribirLogsVisiblesUsuario("Busqueda realizada con éxito.");
                }
                else
                {
                    Helpers.EscribirLogsVisiblesUsuario("La busqueda no obtuvo resultados.");
                    return 0;
                }
                var listaNumerosViaje = new List<string>();
                Helpers.AgregarInformacionNumeroViaje("Tipo de reporte: Aéreo Viajes:");
                //Helpers.EscribirLogsVisiblesUsuario("Procesando "+filas.Count+ " viajes de la página #"+numeroPagina);
                foreach (var fila in filas)
                {
                    var columna = fila.FindElement(By.XPath("td[2]"));
                    //var numeroViaje = columna.FindElement(By.TagName("span")).Text;
                    var numeroViaje = columna.FindElement(By.TagName("span"));
                    var numeroEnTexto = columna.FindElement(By.TagName("a")).Text;
                    numViaje = numeroEnTexto;

                    //Se verifica si el viaje ya habia sido procesado anteriormente
                    var viajeYaProcesado = Helpers.ObtenerInformacionViajeAereoJsonWebService("GET", ObtenerViajeAereo, numViaje);
                    if (viajeYaProcesado!=null)
                    {
                        if (viajeYaProcesado.FirstOrDefault().lva_procesado == "0")
                        {
                            //Se obtiene la informacion de la fila
                            var fechaCreacion = fila.FindElement(By.XPath("td[10]")).FindElement(By.TagName("span")).Text;
                            Helpers.AgregarInformacionNumeroViaje("Procesando viaje: " + numeroEnTexto + " | " + (contadorFilas + 1) + "/" + filas.Count + " de la página #" + numeroPagina+"| Fecha: "+fechaCreacion);
                            var tipoViaje = fila.FindElement(By.XPath("td[11]")).FindElement(By.TagName("span")).Text;
                            var recintoOrigen = fila.FindElement(By.XPath("td[7]")).FindElement(By.TagName("span")).Text;
                            var recintoDestino = fila.FindElement(By.XPath("td[9]")).FindElement(By.TagName("span")).Text;

                            AbrirNuevoTab(numeroViaje);
                            CambiarTab("Viajes Aereo");

                            //Se extra la informacion de los pesos
                            var pesos = ObtenerPesosOrigenDestinoAereo();

                            if (!numerosViajeMaritimo.Contains(numeroEnTexto))
                            {
                                //Se envia la informacion al WS
                                //Se crea el JSON que se envia al WS
                                var json = new JavaScriptSerializer().Serialize(new
                                {
                                    AV_FCH_CREACION_P = fechaCreacion,
                                    AV_REC_ORIGEN_P = recintoOrigen,
                                    AV_REC_DESTINO_P = recintoDestino,
                                    AV_PESO_ORIGEN_P = pesos["Peso Origen"],
                                    AV_PESO_DESTINO_P = pesos["Peso Destino"],
                                    AV_NUM_VIAJE_P = numeroEnTexto,
                                    AV_TIPO_VIAJE_P = tipoViaje
                                });
                                var codigoHttp = Helpers.LLamarWebService("POST", insertaAereoViajes, json.ToUpper());
                                if (codigoHttp=="OK")
                                {
                                    numerosViajeMaritimo.Add(numeroEnTexto);
                                    //Se actualiza la fila del viaje en la lista previamente descargada con todos los viajes proyectados a ser procesados
                                    json = new JavaScriptSerializer().Serialize(new
                                    {
                                        NUM_VIAJE_P = numViaje,
                                        PROCESADO_P = "1"
                                    });
                                    Helpers.LLamarWebService("POST", ActualizarListaViajeAereo, json);
                                }
                            }

                            //Se escribe la informacion en el txt como prueba para guardar en base de datos con los web services
                            Helpers.AgregarLineaViajeAereo("(" + (contadorFilas+1) + ") -> Fecha Creación: " + fechaCreacion + "-- Recinto de Origen" + recintoOrigen + "-- Recinto de Destino" + recintoDestino + "-- Peso Origen" + pesos["Peso Origen"]+ "-- Peso Destino" + pesos["Peso Destino"]);

                            Helpers.AgregarLineaDUA("Procesando viaje #" + numeroEnTexto + ":\n");
                            Helpers.EscribirLogsVisiblesUsuario("Procesando viaje #" + numeroEnTexto+" de la página #" + numeroPagina + "| Fecha: " + fechaCreacion);

                            cerrarTab();

                            contadorFilas++;
                        }
                        else if(viajeYaProcesado.FirstOrDefault().lva_procesado == "1")
                        {
                            Helpers.EscribirLogsVisiblesUsuario($"El viaje {numViaje} ya habia sido procesado.");
                        }
                    }

                }

                Helpers.EscribirLogsVisiblesUsuario("Viajes procesados exitosamente: "+ contadorFilas +" de "+ filas.Count+" de la página #"+numeroPagina);


            }
            catch (Exception ex)
            {
                Logger.addLog("ID: " + runID + ": " + ex.Message + " || Paso: Obtencion de datos en tabla de viajes Aéreo para número de viaje "+ numViaje);
            }

            return contadorFilas;
        }

        public Dictionary<string, double> ObtenerPesosOrigenDestinoAereo() 
       {

            Dictionary<string, double> pesos = new Dictionary<string, double>();
            int cont =0;
            double pesoOrigen = 0;
            double pesoDestino = 0;
            try
            {
                //Se busca la tabla y se obtienen las lineas del cuerpo de la misma. 
                var filas = wait.Until(e=>e.FindElement(By.Id("Grid10ContainerTbl"))).FindElement(By.TagName("tbody")).FindElements(By.TagName("tr"));
                foreach (var fila in filas)
                {
                    var recinto = fila.FindElement(By.XPath("td[2]")).FindElement(By.TagName("span")).Text; 
                    var pesoString = fila.FindElement(By.XPath("td[5]")).FindElement(By.TagName("span")).Text;
                    //var peso = double.Parse(pesoString.Replace(".000", ""));
                    var str = pesoString.Remove(pesoString.Length - 1);
                    //var peso = double.Parse(str, System.Globalization.CultureInfo.InvariantCulture);
                    //var peso = Convert.ToDouble(str.Replace(".", ","), System.Globalization.CultureInfo.InvariantCulture);
                    var peso = Convert.ToDouble(str, System.Globalization.CultureInfo.InvariantCulture);
                    //Si el contador es menor o igual que el numero de filas, quiere decir que estamos viendo los pesos de origen. 
                    if (cont<(filas.Count/2))
                    {
                        pesoOrigen +=peso;
                    }
                    else
                    {

                        pesoDestino += peso;
                    }
                    cont++;
                }
            }
            catch (Exception ex)
            {

                Logger.addLog("ID: " + runID + ": " + ex.Message + " || Paso: Obtencionde pesos Origen/Destino Aéreo");
            }
            pesos.Add("Peso Origen", pesoOrigen);
            pesos.Add("Peso Destino", pesoDestino);

            return pesos;
            
        }
        #endregion

        #region Reporte Aereo Manifiestos

        //Variables para reporte manifiesto aereo

        /// <summary>
        /// Nombre del transportista del manifiesto
        /// </summary>
        string nombreTransportista = string.Empty;

        /// <summary>
        /// Buscar los manifiestos para el reporte aéreo
        /// </summary>
        /// <param name="fechaInicio"></param>
        /// <param name="fechaFinal"></param>
        public void BuscarManifiestosAereo(string fechaInicio, string fechaFinal, bool esPrimeraBusqueda = true) 
        {
            try
            {
                if (esPrimeraBusqueda)
                {
                    Helpers.EscribirLogsVisiblesUsuario("Buscando Manifiestos para reporte Aéreo");
                }
                //Se limpian los campos de fechas, Nro Manifiesto y Ubicacion Ing/Sal
                var nroManifiesto = wait.Until(e => e.FindElement(By.Id("vVCGNROMIC")));
                nroManifiesto.Click();
                nroManifiesto.Clear();

                var fechaInicioField = wait.Until(e => e.FindElement(By.Id("vVFCH1")));
                fechaInicioField.Click();
                fechaInicioField.Clear();

                var fechaFinalField = wait.Until(e => e.FindElement(By.Id("vVFCHF")));
                fechaFinalField.Click();
                fechaFinalField.Clear();

                var ubicacioneIngSalField = wait.Until(e => e.FindElement(By.Id("vRGDEPID")));
                ubicacioneIngSalField.Click();
                ubicacioneIngSalField.Clear();

                //Se ingresa la Ubicacion Ing/Sal
                ubicacioneIngSalField.Click();
                ubicacioneIngSalField.SendKeys("P001");

                //Se ingresan las fechas
                fechaInicioField.Click();
                fechaInicioField.SendKeys(fechaInicio);

                fechaFinalField.Click();
                fechaFinalField.SendKeys(fechaFinal);

                //Modalidad de transporte
                var modalidadTransporte = wait.Until(e => e.FindElement(By.Id("vVTRANS")));
                var selectElement = new SelectElement(modalidadTransporte);
                selectElement.SelectByValue("4");

                //Tipo de manifiesto 
                var tipoManifiesto = wait.Until(e => e.FindElement(By.Id("vTIPOMANIFIESTO")));
                var selectElementTipoManifiesto = new SelectElement(tipoManifiesto);
                selectElementTipoManifiesto.SelectByValue("0");
                
                var btn = wait.Until(e=>e.FindElement(By.Name("BTN_ENTER")));

                Thread.Sleep(2000);

                btn.Click();
            }
            catch (Exception ex)
            {
                Logger.addLog("ID: " + runID + ": " + ex.Message + " || Paso: Busqueda de Manifiestos Aéreo");
            }
        }

        public void ObtenerListaAereoManifiesto(IReadOnlyCollection<IWebElement> filas) 
        {
            try
            {
                Helpers.EscribirLogsVisiblesUsuario($"Cantidad de manifiestos a procesar: {filas.Count}");
                foreach (var fila in filas)
                {
                    var numeroManifiesto = wait.Until(c => fila.FindElement(By.XPath("td[3]")).FindElement(By.TagName("span")).Text);
                    var fechaAereoManifiesto = wait.Until(c => fila.FindElement(By.XPath("td[7]")).FindElement(By.TagName("span")).Text);
                    //Se crea el JSON que se envia al WS
                    var json = new JavaScriptSerializer().Serialize(new
                    {
                        LM_NUM_MANIFIESTO_P = numeroManifiesto,
                        LM_FECHA_P = fechaAereoManifiesto
                    });

                    Helpers.LLamarWebService("POST", InsertarListaManifiestos, json);

                }
            }
            catch (Exception ex)
            {

                Logger.addLog("ID: " + runID + ": " + ex.Message + " en ObtenerListaAereoManifiesto()");
            } 
        }

        string manifiestoActual = string.Empty;
        public void RecorrerTablaManifiestosAereo(bool reanudacion) 
        {
            try
            {
                tabManifiestoAereo = driver.CurrentWindowHandle;
                Helpers.EscribirLogsVisiblesUsuario("Busqueda finalizada y exitosa");
                Thread.Sleep(3000);
                var informacionTablaManifiestos= wait.Until(e => e.FindElement(By.Id("Grid1ContainerTbl")));
                var tbody = wait.Until(c => informacionTablaManifiestos.FindElement(By.TagName("tbody")));
                var filas = wait.Until(c => tbody.FindElements(By.TagName("tr")));
                //Se obtiene la cantidad total de manifiestos a ser procesados
                if (!reanudacion)
                {
                    ObtenerListaAereoManifiesto(filas);
                }
                var contadorFilas = 0;
                var listaManifiestos = new List<string>();
                Helpers.AgregarInformacionNumeroManifiestoAereo("Tipo de reporte: Aéreo Manifiestos:");
                Helpers.AgregarInformacionNumeroManifiestoAereo("Procesando "+filas.Count+" manifiestos");
                Helpers.EscribirLogsVisiblesUsuario("Procesando " + filas.Count + " manifiestos encontrados");
                foreach (var fila in filas)
                {
                    var numeroManifiesto = wait.Until(c => fila.FindElement(By.XPath("td[3]")).FindElement(By.TagName("span")).Text);
                    var viajeYaProcesado = Helpers.ObtenerInformacionManifiestoAereoJsonWebService("GET", ObtenerManifiestoAereo, numeroManifiesto);
                    if (viajeYaProcesado.FirstOrDefault().lm_procesado=="0")
                    {
                        fechaAereoManifiesto = wait.Until(c => fila.FindElement(By.XPath("td[7]")).FindElement(By.TagName("span")).Text);
                        manifiestoActual = numeroManifiesto;
                        Helpers.AgregarInformacionNumeroManifiestoAereo("Procesando Manifiesto: " + numeroManifiesto + " | " + (contadorFilas + 1) + "/" + filas.Count+" | Fecha: "+ fechaAereoManifiesto);

                        Helpers.EscribirLogsVisiblesUsuario("Procesando Manifiesto: " + numeroManifiesto + " | " + (contadorFilas + 1) + "/" + filas.Count + " | Fecha: " + fechaAereoManifiesto);
                        ObtenerInformacionManifiestosAereo(fila);
                    }
                    else if(viajeYaProcesado.FirstOrDefault().lm_procesado == "1")
                    {
                        Helpers.EscribirLogsVisiblesUsuario($"El manifiesto {numeroManifiesto} ya habia sido procesado.");
                    }

                    contadorFilas++;
                }
                Helpers.AgregarInformacionNumeroManifiestoAereo("Manifiestos procesados: "+contadorFilas+ " de "+filas.Count);
                Helpers.EscribirLogsVisiblesUsuario("Manifiestos procesados exitosamente: " + contadorFilas + " de " + filas.Count + " | Fecha: " + fechaAereoManifiesto);

            }
            catch (Exception ex)
            {
                Logger.addLog("ID: " + runID + ": " + ex.Message + " || Paso: Recorrido de tabla de manifiestos Aéreo");
            }
        }

        /// <summary>
        /// Se encarga de entrar en cada uno de los manifiestos para extraer la informacion necesaria
        /// </summary>
        /// <param name="fila"></param>
        public void ObtenerInformacionManifiestosAereo(IWebElement fila) 
        {
            try
            {
                var conocimiento = wait.Until(e => fila.FindElement(By.XPath("td[8]")).FindElement(By.TagName("a")));
                nombreTransportista = wait.Until(e => fila.FindElement(By.XPath("td[17]")).FindElement(By.TagName("span"))).Text;
                AbrirNuevoTab(conocimiento);
                CambiarTab("Manifiestos Aereo");
                ProcesarResultadosConocimientosEmbarque();
                cerrarTab();

            }
            catch (Exception ex)
            {
                Logger.addLog("ID: " + runID + ": " + ex.Message + " || Paso: Obtención de información manifiestos Aéreo");
            }
        }

        /// <summary>
        /// Se recorre toda la tabla de cocimientos de embarque.
        /// </summary>
        public void ProcesarResultadosConocimientosEmbarque() 
        {
            try
            {
                var hayOtraPagina = true;
                var numeroPagina = 1;
                while (hayOtraPagina)
                {
                    //Si es falso es porque no hay lineas que recorrer
                    if (!RecorrerTablaConocimientosEmbarque(numeroPagina))
                    {
                        return;
                    }

                    RecorrerTablaConocimientosEmbarque(numeroPagina);
                    hayOtraPagina = SeleccionarSiguientePaginaTabla(numeroPagina);
                    numeroPagina++;
                }
            }
            catch (Exception ex)
            {

                Logger.addLog("ID: " + runID + ": " + ex.Message + " || Paso: Procesamiento de resultados de conocimientos de embarque");
            }
        }

        string numeroGuia = string.Empty;
        string tipoGuia = string.Empty;

        string numeroGuiaHija = string.Empty;

        /// <summary>
        /// Peso que se toma de la linea de mercancia
        /// </summary>
        double pesoLineaMercancia = 0;
        string depDestLineaMercancia = string.Empty;
        string ubicacionLineaMercancia = string.Empty;
        string estadoLineaMercancia = string.Empty;
        string marcasLineaMercancia = string.Empty;
        double totalBultosLineaMercancia = 0;
        /// <summary>
        /// Se encarga de obtener las filas de la tabla para poder procesarlas 
        /// </summary>
        public bool RecorrerTablaConocimientosEmbarque(int numeroPagina) 
        {
            Thread.Sleep(2000);
            var hayLineas = false;
            try
            {
                var tabla = wait.Until(e=>e.FindElement(By.Id("Grid1ContainerTbl")));
                var body = wait.Until(e=>tabla.FindElement(By.TagName("tbody")));
                var lineas = wait.Until(e => body.FindElements(By.TagName("tr")));
                var index = 1;
                //Si no hay lineas, quiere decir que no hay conocimientos de embarque para ese manifiesto
                if (lineas.Count==0)
                {
                    //Se envia la informacion al WS
                    //Se crea el JSON que se envia al WS
                    var json = new JavaScriptSerializer().Serialize(new
                    {
                        AM_RAZON_SOCIAL_P = "-",
                        AM_PESO_P = 0,
                        AM_DESTINO_P = "-",
                        AM_NUM_MOVIMIENTO_P = 0,
                        AM_TOTAL_BULTOS_P = 0,
                        AM_MANIFIESTO_P = manifiestoActual,
                        AM_TRANSPORTISTA_P = nombreTransportista,
                        AM_NUMERO_GUIA_P = "-",
                        AM_TIPO_GUIA_P = "-",
                        AM_NUMERO_GUIA_HIJA_P = "-",
                        AM_FECHA_P = fechaAereoManifiesto,
                        AM_DEP_DEST_P = "-",
                        AM_UBICACION_P = "-",
                        AM_ESTADO_P = "-",
                        AM_MARCAS_P = "-",
                        AM_DESC_ANTICIPADOS_P = "-"
                    });
                    Helpers.LLamarWebService("POST", insertaAereoManifiestos, json.ToUpper());
                    json = new JavaScriptSerializer().Serialize(new
                    {
                        NUM_MANIFIESTO_P = manifiestoActual,
                        PROCESADO_P = "1"
                    });
                    Helpers.LLamarWebService("POST", ActualizarListaAereoManifiesto, json.ToUpper());
                }
                foreach (var linea in lineas)
                {

                    hayLineas = true;

                    numeroGuia = wait.Until(e => linea.FindElement(By.XPath("td[4]"))).FindElement(By.TagName("span")).Text;

                    //Se verifica si ya la guia fue procesada
                    var numGuiaObj = Helpers.ObtenerNumeroGuiaManifiestoJsonWebService("GET", ObtenerNumGuia, numeroGuia);
                    if (numGuiaObj.Count == 0) 
                    {
                        tipoGuia = wait.Until(e => linea.FindElement(By.XPath("td[7]"))).FindElement(By.TagName("span")).Text;
                        //Se limpia el valor de esta variable 
                        numeroGuiaHija = string.Empty;

                        //Se obtiene la columna llamada "Master" para determinar si tiene hijos o no 
                        var columnaMaster = wait.Until(e => linea.FindElement(By.XPath("td[15]"))).FindElement(By.TagName("span"));
                        var columnaMasterTexto = columnaMaster.Text;
                        Helpers.AgregarInformacionNumeroManifiestoAereo("Procesando conocimiento de embarque: " + columnaMasterTexto + " | " + (index) + "/" + lineas.Count + " de la página #" + numeroPagina);
                        //Si la columna tiene hijos se ingresa al link
                        if (columnaMasterTexto == "Hijos")
                        {
                            ObtenerInformacionHijosManifiesto(columnaMaster);
                        }
                        //Si no hay nada en la columna de "Master", se ingresa directamente al link de lineas ya que es una guia HWB sin padre
                        if (columnaMasterTexto == string.Empty)
                        {

                            var lineasLink = wait.Until(e => linea.FindElement(By.XPath("td[2]")).FindElement(By.TagName("a")));
                            AbrirNuevoTab(lineasLink);
                            CambiarTab("Manifiestos Aereo", "Guia HWB");
                            Thread.Sleep(1000);
                            var afectacionesLink = wait.Until(e => e.FindElement(By.Id("Grid1ContainerRow_0001"))).FindElement(By.XPath("td[3]")).FindElement(By.TagName("a"));
                            AbrirNuevoTab(afectacionesLink);
                            CambiarTab("Manifiestos Aereo");
                            RecorrerMovimientosStock();
                            wait.Until(e => e.SwitchTo().Window(tabConocimientosEmbarque));
                        }
                        index++;
                    }
                    

                }
            }
            catch (Exception ex)
            {

                Logger.addLog("ID: " + runID + ": " + ex.Message + " || Paso: Recorrido de tabla de conocimientos de embarque");
            }
            return hayLineas;
        }

        /// <summary>
        /// Se obtiene la informacion de los hijos manifiesto para el reporte aereo. 
        /// </summary>
        /// <param name="columnaMaster"></param>
        public void ObtenerInformacionHijosManifiesto(IWebElement columnaMaster) 
        {
            try
            {
                AbrirNuevoTab(columnaMaster);
                CambiarTab("Manifiestos Aereo");
                //Se ingresa al link de lineas para cada uno de los resultados
                RecorrerTablaLineasHijosManifiesto();
                //wait.Until(e => e.SwitchTo().Window(tabHijosConocimientos));
                //driver.Close();
                wait.Until(e => e.SwitchTo().Window(tabConocimientosEmbarque));
            }
            catch (Exception ex)
            {

                Logger.addLog("ID: " + runID + ": " + ex.Message + " || Paso: Obtención de datos para resultados con hijos de conocimiento de embarque en reporte aéreo.");
            }
        }

        /// <summary>
        /// Se encarga de recorrer la tabla que aparece despues de consultar los hijos de una conocimiento de embarque
        /// </summary>
        public void RecorrerTablaLineasHijosManifiesto() 
        {
            try
            {
                var tabla = wait.Until(e => e.FindElement(By.Id("Grid2ContainerTbl")));
                var body = wait.Until(e => tabla.FindElement(By.TagName("tbody")));
                var lineas = wait.Until(e => body.FindElements(By.TagName("tr")));
                var cont = 0;
                foreach (var linea in lineas)
                {
                    wait.Until(e => e.SwitchTo().Window(tabHijosConocimientos));
                    //Se obtiene el link bajo la columna llamada "lineas"
                    var lineasLink = wait.Until(e => linea.FindElement(By.XPath("td[1]")).FindElement(By.TagName("a")));
                    //Se asigna el valor de la guia hija 
                    numeroGuiaHija = wait.Until(e => linea.FindElement(By.XPath("td[3]")).FindElement(By.TagName("span")).Text);

                    AbrirNuevoTab(lineasLink);
                    CambiarTab("Manifiestos Aereo");
                    //Se ingresa a la opcion de "Afectaciones"
                    Thread.Sleep(1000);
                    var afectacionesLink = wait.Until(e => e.FindElement(By.Id("Grid1ContainerRow_0001"))).FindElement(By.XPath("td[3]")).FindElement(By.TagName("a"));

                    //Se obtiene informacion de peso, marcas, Dep.Dest, estado
                    var pesoStr = wait.Until(e => e.FindElement(By.Id("Grid1ContainerRow_0001"))).FindElement(By.XPath("td[8]")).FindElement(By.TagName("span")).Text;
                    var psStr = pesoStr.Remove(pesoStr.Length - 1);
                    var peso = double.Parse(psStr, System.Globalization.CultureInfo.InvariantCulture);
                    //var peso = Convert.ToDouble(psStr.Replace(".", ","));
                    pesoLineaMercancia = peso;

                    var totalBultosStr = wait.Until(e => e.FindElement(By.Id("Grid1ContainerRow_0001"))).FindElement(By.XPath("td[10]")).FindElement(By.TagName("span")).Text;
                    var tbStr = totalBultosStr.Remove(totalBultosStr.Length - 1);
                    var totalBultos = double.Parse(tbStr, System.Globalization.CultureInfo.InvariantCulture);
                    totalBultosLineaMercancia = totalBultos;
                    depDestLineaMercancia = wait.Until(e => e.FindElement(By.Id("Grid1ContainerRow_0001"))).FindElement(By.XPath("td[12]")).FindElement(By.TagName("span")).Text;
                    ubicacionLineaMercancia = wait.Until(e => e.FindElement(By.Id("Grid1ContainerRow_0001"))).FindElement(By.XPath("td[13]")).FindElement(By.TagName("span")).Text;
                    marcasLineaMercancia = wait.Until(e => e.FindElement(By.Id("Grid1ContainerRow_0001"))).FindElement(By.XPath("td[15]")).FindElement(By.TagName("span")).Text;
                    estadoLineaMercancia = wait.Until(e => e.FindElement(By.Id("Grid1ContainerRow_0001"))).FindElement(By.XPath("td[16]")).FindElement(By.TagName("span")).Text;
                    AbrirNuevoTab(afectacionesLink);
                    CambiarTab("Manifiestos Aereo");
                    RecorrerMovimientosStock();

                    cont++;

                }
                wait.Until(e => e.SwitchTo().Window(tabHijosConocimientos));
                driver.Close();
                //driver.SwitchTo().Window(tabConocimientosEmbarque);
            }
            catch (Exception ex)
            {

                Logger.addLog("ID: " + runID + ": " + ex.Message + " || Paso: Recorrido de la tabla de resultados de los hijos de conocimiento de embarque.");
            }
        }

        /// <summary>
        /// Recorremos la tabla de Movimientos de stock
        /// </summary>
        public void RecorrerMovimientosStock() 
        {
            try
            {
                var tabla = wait.Until(e => e.FindElement(By.Id("SubfilemovContainerTbl")));
                var body = wait.Until(e => tabla.FindElement(By.TagName("tbody")));
                var lineas = wait.Until(e => body.FindElements(By.TagName("tr")));
                foreach (var linea in lineas)
                {
                    //Si hay mas de una linea, se confirma que estamos en el tab correcto para seguir procesando las mismas.
                    if (lineas.Count>1)
                    {
                        driver.SwitchTo().Window(tabAfectacionesPorLinea);
                    }
                    var movimientoLink = wait.Until(e => linea.FindElement(By.XPath("td[2]"))).FindElement(By.TagName("span"));
                    AbrirNuevoTab(movimientoLink);
                    CambiarTab("Manifiestos Aereo");
                    ObtenerInformacionMovimientoStock();

                }
                driver.SwitchTo().Window(tabAfectacionesPorLinea);
                driver.Close();
                wait.Until(e => e.SwitchTo().Window(tabLineasMercancia));
                //Aqui le ingresa la informacion en caso de que no haya lineas de movimientos
                //Si no existe un movimiento se agrega un record con un flag(Guiones en varios de los campos)
                if (lineas.Count == 0)
                {
                    var descripcionComercialAnticipados = ObtenerDescripcionAnticipados();
                    CerrarTabsDescripcionAnticipado();
                    ObtenerInformacionManifiestoSinMovimientos(descripcionComercialAnticipados);

                }
                driver.Close();
            }
            catch (Exception ex)
            {

                Logger.addLog("ID: " + runID + ": " + ex.Message + " || Paso: Recorrido de la tabla de movimientos stock en reporte aereo.");
            }
        }

        public void ObtenerInformacionMovimientoStock() 
        {
            var manifiesto = string.Empty;
            try
            {
                var fila = wait.Until(e => e.FindElement(By.Id("Grid1ContainerRow_0001")));
                var razonSocial = wait.Until(e => fila.FindElement(By.XPath("td[22]"))).FindElement(By.TagName("span")).Text;
                //Peso
                var pesoStr = wait.Until(e => fila.FindElement(By.XPath("td[13]"))).FindElement(By.TagName("span")).Text;
                var psStr = pesoStr.Remove(pesoStr.Length - 1);
                var peso = double.Parse(psStr, System.Globalization.CultureInfo.InvariantCulture);
                //var peso = Convert.ToDouble(psStr.Replace(".", ","), System.Globalization.CultureInfo.InvariantCulture);

                manifiesto = wait.Until(e => fila.FindElement(By.XPath("td[9]"))).FindElement(By.TagName("span")).Text;

                var destino = wait.Until(e => e.FindElement(By.Id("span_vVRGRSOC"))).Text;
                var numeroMovimiento = wait.Until(e => fila.FindElement(By.XPath("td[6]"))).FindElement(By.TagName("span")).Text;
                var numeroMovimientoInt = int.Parse(numeroMovimiento);
                //Total de bultos
                var totalBultosStr = wait.Until(e => fila.FindElement(By.XPath("td[15]"))).FindElement(By.TagName("span")).Text;
                var tbStr = totalBultosStr.Remove(totalBultosStr.Length - 1);
                var totalBultos = double.Parse(tbStr, System.Globalization.CultureInfo.InvariantCulture);

                var estadoMovimiento = wait.Until(e => fila.FindElement(By.XPath("td[23]"))).FindElement(By.TagName("span")).Text;

                //Se envia la informacion al WS
                //Se crea el JSON que se envia al WS
                var json = new JavaScriptSerializer().Serialize(new
                {
                    AM_RAZON_SOCIAL_P = razonSocial,
                    AM_PESO_P = peso,
                    AM_DESTINO_P = destino,
                    AM_NUM_MOVIMIENTO_P = numeroMovimientoInt,
                    AM_TOTAL_BULTOS_P = totalBultos,
                    AM_MANIFIESTO_P = manifiesto,
                    AM_TRANSPORTISTA_P = nombreTransportista,
                    AM_NUMERO_GUIA_P = numeroGuia,
                    AM_TIPO_GUIA_P = tipoGuia, 
                    AM_NUMERO_GUIA_HIJA_P = numeroGuiaHija,
                    AM_FECHA_P = fechaAereoManifiesto,
                    AM_DEP_DEST_P = depDestLineaMercancia,
                    AM_UBICACION_P = ubicacionLineaMercancia,
                    AM_ESTADO_P = estadoLineaMercancia,
                    AM_MARCAS_P = marcasLineaMercancia,
                    AM_DESC_ANTICIPADOS_P = "-",
                    AM_ESTADO_MOVIMIENTO_P = estadoMovimiento
                });
                Helpers.LLamarWebService("POST", insertaAereoManifiestos, json.ToUpper());
                json = new JavaScriptSerializer().Serialize(new
                {
                    NUM_MANIFIESTO_P = manifiestoActual,
                    PROCESADO_P = "1"
                });
                Helpers.LLamarWebService("POST", ActualizarListaAereoManifiesto, json.ToUpper());

                driver.SwitchTo().Window(tabMovimientoStock);
                driver.Close();
            }
            catch (Exception ex)
            {

                Logger.addLog("ID: " + runID + ": " + ex.Message + " || Paso: Obtencion de movimiento stock para manifiesto "+manifiesto);
            }
        }

        public void ObtenerInformacionManifiestoSinMovimientos(string descripcionComercialAnticipados) 
        {
            try
            {
                var fila = wait.Until(e => e.FindElement(By.Id("Grid1ContainerRow_0001")));
                //Peso
                var pesoStr = wait.Until(e => fila.FindElement(By.XPath("td[8]"))).FindElement(By.TagName("span")).Text;
                var psStr = pesoStr.Remove(pesoStr.Length - 1);
                //var peso = Convert.ToDouble(psStr.Replace(".", ","), System.Globalization.CultureInfo.InvariantCulture);
                var peso = Convert.ToDouble(psStr, System.Globalization.CultureInfo.InvariantCulture);

                //Total de bultos
                var totalBultosStr = wait.Until(e => fila.FindElement(By.XPath("td[10]"))).FindElement(By.TagName("span")).Text;
                var tbStr = totalBultosStr.Remove(totalBultosStr.Length - 1);
                var totalBultos = double.Parse(tbStr, System.Globalization.CultureInfo.InvariantCulture);

                depDestLineaMercancia = wait.Until(e => fila.FindElement(By.XPath("td[12]"))).FindElement(By.TagName("span")).Text;

                ubicacionLineaMercancia = wait.Until(e => fila.FindElement(By.XPath("td[13]"))).FindElement(By.TagName("span")).Text;

                estadoLineaMercancia = wait.Until(e => fila.FindElement(By.XPath("td[16]"))).FindElement(By.TagName("span")).Text;

                marcasLineaMercancia = wait.Until(e => fila.FindElement(By.XPath("td[15]"))).FindElement(By.TagName("span")).Text;


                var json = new JavaScriptSerializer().Serialize(new
                {
                    AM_RAZON_SOCIAL_P = "-",
                    AM_PESO_P = peso,
                    AM_DESTINO_P = "-",
                    AM_NUM_MOVIMIENTO_P = 0,
                    AM_TOTAL_BULTOS_P = totalBultos,
                    AM_MANIFIESTO_P = manifiestoActual,
                    AM_TRANSPORTISTA_P = nombreTransportista,
                    AM_NUMERO_GUIA_P = numeroGuia,
                    AM_TIPO_GUIA_P = tipoGuia,
                    AM_NUMERO_GUIA_HIJA_P = numeroGuiaHija,
                    AM_FECHA_P = fechaAereoManifiesto,
                    AM_DEP_DEST_P = depDestLineaMercancia,
                    AM_UBICACION_P = ubicacionLineaMercancia,
                    AM_ESTADO_P = estadoLineaMercancia,
                    AM_MARCAS_P = marcasLineaMercancia,
                    AM_DESC_ANTICIPADOS_P = descripcionComercialAnticipados
                });
                Helpers.LLamarWebService("POST", insertaAereoManifiestos, json.ToUpper());
                json = new JavaScriptSerializer().Serialize(new
                {
                    NUM_MANIFIESTO_P = manifiestoActual,
                    PROCESADO_P = "1"
                });
                Helpers.LLamarWebService("POST", ActualizarListaAereoManifiesto, json.ToUpper());

            }
            catch (Exception ex)
            {

                Logger.addLog($"ID: {runID}:   {ex.Message} || ObtenerInformacionManifiestoSinMovimientos()");
            }
        }


        public string ObtenerDescripcionAnticipados() 
        {
            try
            {
                var fila = wait.Until(e => e.FindElement(By.Id("Grid1ContainerRow_0001")));
                var afectaciones = fila.FindElement(By.XPath("td[3]")).FindElement(By.TagName("a"));
                afectaciones.Click();
                CambiarTab("Manifiestos Aereo", "DescripcionAnticipadosTab1");

                var dua = wait.Until(e => e.FindElement(By.Id("span_vNUME_CORRE_0001"))).FindElement(By.TagName("a"));
                dua.Click();
                CambiarTab("Manifiestos Aereo", "DescripcionAnticipadosTab2");

                var element = wait.Until(e => e.FindElement(By.XPath("//input[@value='Lineas del DUA']")));
                element.Click();
                CambiarTab("Manifiestos Aereo", "DescripcionAnticipadosTab3");
                var descripcionComercial = wait.Until(e => e.FindElement(By.Id("span_vDESC_COMER_0001"))).Text;

                return descripcionComercial;
            }
            catch (Exception ex)
            {

                Logger.addLog($"ID: {runID}:   {ex.Message} || ObtenerDescripcionAnticipados()");
            }
            return string.Empty;
        }

        public bool CerrarTabsDescripcionAnticipado()
        {
            try
            {
                foreach (var tab in listaTabsDescripcionAnticipados.Distinct())
                {
                    if (tab!=tabLineasMercancia)
                    {
                        wait.Until(e => e.SwitchTo().Window(tab));
                        driver.Close();
                    }
                }

                wait.Until(e => e.SwitchTo().Window(tabAfectacionesPorLinea));
                driver.Close();
                wait.Until(e => e.SwitchTo().Window(tabLineasMercancia));
                listaTabsDescripcionAnticipados.Clear();
                return true;

            }
            catch (Exception ex)
            {
                Logger.addLog("ID: " + runID + ": " + ex.Message + " en CerrarTabsRedestino()");
                return false;
            }
        }
        #endregion

        #endregion


    }
}
